from rest_framework import serializers
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin

from .models import Player


class PlayerSerializer(SerializerExtensionsMixin, serializers.ModelSerializer):
    birth_date = serializers.DateField(input_formats=['iso-8601', '%d.%m.%Y'])

    class Meta:
        model = Player
        fields = ('first_name', 'last_name', 'name',
                  'id', 'birth_date', 'number')
        read_only_fields = ('id',)
        expandable_fields = dict(
            team=dict(
                serializer='api.team.serializers.TeamSerializer',
                read_only=False
            )
        )
