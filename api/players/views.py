# Create your views here.
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.decorators import detail_route
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin

from api.enums import TeamStateTypes
from api.team.models import Team
from .filters import PlayerFilter
from .models import Player
from .serializers import PlayerSerializer


class PlayerViewSet(SerializerExtensionsAPIViewMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Player.objects.all().exclude(team__state=TeamStateTypes.denied)
    permission_classes = (IsAuthenticated,)
    serializer_class = PlayerSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = PlayerFilter
    pagination_class = None

    @detail_route(methods=['get'])
    def get(self, request, **kwargs):
        team_id = request.query_params.get('team', None)
        mine = request.query_params.get('mine', None)
        queryset = Player.objects.all()

        if team_id is not None:
            team = get_object_or_404(Team.objects.all(), pk=team_id)

            if request.user.is_staff is False and request.user != team.trainer:
                raise PermissionDenied

            queryset = queryset.filter(team=team)
        elif mine is not None:
            queryset = queryset.filter(team__trainer=request.user)
        else:
            if not request.user.is_staff:
                raise PermissionDenied

        return Response(PlayerSerializer(queryset, many=True).data, status=status.HTTP_200_OK)
