from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from .models import Game
from .tasks import update_team_and_group


@receiver(pre_save, sender=Game)
def pre_save_game(sender, instance, **kwargs):
    instance.is_played = instance.get_is_played()

    instance.points_a = instance.get_points_a()
    instance.points_b = instance.get_points_b()
    instance.sets_a = instance.count_halftimes_won('A')
    instance.sets_b = instance.count_halftimes_won('B')

    instance.goals_a = (instance.scoreAHalftime1 or 0) \
        + (instance.scoreAHalftime2 or 0) \
        + (instance.scoreAHalftime3 or 0)
    instance.goals_b = (instance.scoreBHalftime1 or 0) \
        + (instance.scoreBHalftime2 or 0) \
        + (instance.scoreBHalftime3 or 0)

    if instance.is_played:
        if instance.did_win('A', 'B'):
            instance.winner = instance.teamA
        elif instance.did_win('B', 'A'):
            instance.winner = instance.teamB
        else:
            instance.winner = None


@receiver(post_save, sender=Game)
def post_save_game(sender, instance, **kwargs):
    if not instance.is_empty:
        update_team_and_group.delay(instance.id)
