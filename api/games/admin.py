from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Game


class GameAdmin(admin.ModelAdmin):
    list_display = ('number', 'datetime', 'court', 'group_name', 'tournament_name', 'event',
                    'teamA_name', 'teamB_name', '_is_played', 'points_a', 'points_b')
    fieldsets = (
        (_('General'), {'fields': ('number', 'datetime', 'court', 'group', 'event', 'referees')}),
        (_('Teams'), {'fields': ('teamA', 'teamB')}),
        (_('Halftime 1'), {'fields': ('scoreAHalftime1', 'scoreBHalftime1')}),
        (_('Halftime 2'), {'fields': ('scoreAHalftime2', 'scoreBHalftime2')}),
        (_('Halftime 3'), {'fields': ('scoreAHalftime3', 'scoreBHalftime3')}),
    )
    list_filter = ('event', 'group__tournament', 'group')

    def tournament_name(self, obj):
        return str(obj.group.tournament) if obj.has_group else None

    tournament_name.short_description = _('Tournament')

    def group_name(self, obj):
        return obj.group.name if obj.has_group else None

    group_name.short_description = _('Group')

    def teamA_name(self, obj):
        return obj.teamA.name if obj.has_team_a else None

    teamA_name.short_description = _('team A')

    def teamB_name(self, obj):
        return obj.teamB.name if obj.has_team_b else None

    teamB_name.short_description = _('team B')

    def _is_played(self, obj):
        return obj.is_played

    _is_played.boolean = True
    _is_played.short_description = _('Is Played')


admin.site.register(Game, GameAdmin)
