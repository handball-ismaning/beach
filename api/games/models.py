from django.core.exceptions import ValidationError
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from api.enums import GameModeTypes
from api.events.models import Event
from api.groups.models import Group
from api.teamreferences.models import TeamReference


class Game(models.Model):
    number = models.PositiveIntegerField(help_text=_("number of the game"), verbose_name=_("Game number"))
    court = models.PositiveSmallIntegerField(help_text=_("court of the game"),
                                             db_index=True, verbose_name=_("Game court"))
    datetime = models.DateTimeField(help_text=_("date and Time of the game"), verbose_name=_("Game Date & Time"))
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='games',
        db_index=True,
        help_text=_("event of the game"), verbose_name=_("Game Event")
    )
    group = models.ForeignKey(
        Group,
        null=True,
        blank=True,
        db_index=True,
        on_delete=models.CASCADE,
        related_name='games',
        help_text=_("group of the game"), verbose_name=_("Game Group")
    )
    teamA = models.ForeignKey(
        TeamReference,
        null=True,
        blank=True,
        db_index=True,
        on_delete=models.CASCADE,
        related_name='gamesA',
        help_text=_("team A of the game"), verbose_name=_("Team A")
    )
    teamB = models.ForeignKey(
        TeamReference,
        null=True,
        blank=True,
        db_index=True,
        on_delete=models.CASCADE,
        related_name='gamesB',
        help_text=_("team B of the game"), verbose_name=_("Team B")
    )
    points_a = models.PositiveIntegerField(
        help_text=_("points for team A"),
        verbose_name=_("Points Team A"),
        default=0
    )
    points_b = models.PositiveIntegerField(
        help_text=_("points for team B"),
        verbose_name=_("Points Team B"),
        default=0
    )
    sets_a = models.PositiveIntegerField(
        help_text=_("sets won by team A"),
        verbose_name=_("Sets Team A"),
        default=0
    )
    sets_b = models.PositiveIntegerField(
        help_text=_("sets won by team B"),
        verbose_name=_("Sets Team B"),
        default=0
    )
    goals_a = models.PositiveIntegerField(
        help_text=_("Goals made by team A"),
        verbose_name=_("Goals Team A"),
        default=0
    )
    goals_b = models.PositiveIntegerField(
        help_text=_("Goals made by team B"),
        verbose_name=_("Goals Team B"),
        default=0
    )
    winner = models.ForeignKey(
        TeamReference,
        null=True,
        blank=True,
        db_index=True,
        on_delete=models.CASCADE,
        related_name='won_games',
        help_text=_("Winner of the game"), verbose_name=_("Winner")
    )
    is_played = models.BooleanField(
        default=False,
        help_text=_("Is the game already played"), verbose_name=_('Is Played')
    )

    scoreAHalftime1 = models.PositiveSmallIntegerField(
        help_text=_("goals Team A in 1. halftime"),
        verbose_name=_("Goals A 1. Halftime"),
        null=True,
        blank=True
    )
    scoreBHalftime1 = models.PositiveSmallIntegerField(
        help_text=_("goals Team B in 1. halftime"),
        verbose_name=_("Goals B 1. Halftime"),
        null=True,
        blank=True
    )
    scoreAHalftime2 = models.PositiveSmallIntegerField(
        help_text=_("goals Team A in 2. halftime"),
        verbose_name=_("Goals A 2. Halftime"),
        null=True,
        blank=True
    )
    scoreBHalftime2 = models.PositiveSmallIntegerField(
        help_text=_("goals Team B in 2. halftime"),
        verbose_name=_("Goals B 2. Halftime"),
        null=True,
        blank=True
    )
    scoreAHalftime3 = models.PositiveSmallIntegerField(
        help_text=_("goals Team A in 3. halftime"),
        verbose_name=_("Goals A 3. Halftime"),
        null=True,
        blank=True
    )
    scoreBHalftime3 = models.PositiveSmallIntegerField(
        help_text=_("goals Team B in 3. halftime"),
        verbose_name=_("Goals B 3. Halftime"),
        null=True,
        blank=True
    )
    referees = models.CharField(
        blank=True,
        null=True,
        max_length=300,
        help_text=_("Referee Names for the game (separated by whitespace)"),
        verbose_name=_("Referees"))

    @cached_property
    def is_simple_gamemode(self):
        return self.event.game_mode == GameModeTypes.simple

    @cached_property
    def has_team_a(self):
        return hasattr(self, 'teamA') and self.teamA is not None

    @cached_property
    def has_team_b(self):
        return hasattr(self, 'teamB') and self.teamB is not None

    @cached_property
    def has_group(self):
        return hasattr(self, 'group') and self.group is not None

    @cached_property
    def is_empty(self):
        return not self.has_team_a and not self.has_team_b and not self.has_group

    @cached_property
    def is_valid(self):
        return self.has_team_a and self.has_team_b and self.has_group

    def clean(self):
        if not self.is_valid and not self.is_empty:
            raise ValidationError(_('Game needs either both teams and group or needs to be empty'))

        if not self.is_empty:
            if self.teamA.group.tournament != self.group.tournament:
                raise ValidationError(_('Team A needs to belong to the same tournament'))
            if self.teamB.group.tournament != self.group.tournament:
                raise ValidationError(_('Team B needs to belong to the same tournament'))

        if (self.count_halftimes_won('A') + self.count_halftimes_won('B') > 3) or (
                self.count_halftimes_won('A') > 2) or (self.count_halftimes_won('B') > 2):
            raise ValidationError(_('Game Score invalid'))

    def get_is_played(self):
        if self.is_empty:
            return False

        if self.is_simple_gamemode:
            return self.scoreAHalftime1 is not None and self.scoreBHalftime1 is not None
        else:
            if self.scoreAHalftime3 is not None and self.scoreBHalftime3 is not None:
                return True
            else:
                return self.count_halftimes_won('A') == 2 or self.count_halftimes_won('B') == 2

    def get_points_a(self):
        if not self.is_played:
            return 0

        did_win = self.did_win('A', 'B')
        did_loose = self.did_loose('A', 'B')

        if did_win:
            return 2
        elif did_loose:
            return 0
        else:
            return 1

    def get_points_b(self):
        if not self.is_played:
            return 0

        did_win = self.did_win('B', 'A')
        did_loose = self.did_loose('B', 'A')

        if did_win:
            return 2
        elif did_loose:
            return 0
        else:
            return 1

    points_a.short_description = _('Points A')
    points_b.short_description = _('Points B')

    def get_halftime_score(self, player, count):
        return getattr(self, 'score' + player + 'Halftime' + str(count))

    def get_halftime_winner(self, count):
        score_a = self.get_halftime_score('A', count)
        score_b = self.get_halftime_score('B', count)

        if score_a is None or score_b is None:
            return None
        if score_a > score_b:
            return 'A'
        elif score_a < score_b:
            return 'B'
        else:
            return None

    def count_halftimes_won(self, player):
        result = 0
        for x in range(1, 4):
            result = result + (1 if player == self.get_halftime_winner(x) else 0)

        return result

    def did_win(self, player, other_player):
        return self.count_halftimes_won(player) > self.count_halftimes_won(other_player)

    def did_loose(self, player, other_player):
        return self.did_win(other_player, player)

    class Meta:
        verbose_name = _('Game')
        verbose_name_plural = _('Games')
        ordering = ('number',)

    def __str__(self):
        return _('Game Nr. ') + str(self.number)
