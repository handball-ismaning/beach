from django.conf.urls import url

from .views import GameViewSet

game_list = GameViewSet.as_view({
    'get': 'list'
})
game_detail = GameViewSet.as_view({
    'get': 'retrieve',
    'patch': 'partial_update'
})
game_delay = GameViewSet.as_view({
    'post': 'set_delay'
})
game_referees = GameViewSet.as_view({
    'put': 'update_referees'
})

urlpatterns = [
    url(r'^$', game_list, name='game-list'),
    url(r'^(?P<pk>[0-9]+)/set_delay/$', game_delay, name='game-delay'),
    url(r'^(?P<pk>[0-9]+)/update_referees/$', game_referees, name='game-referees'),
    url(r'^(?P<pk>[0-9]+)/$', game_detail, name='game-detail'),
]
