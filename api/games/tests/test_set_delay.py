import json

import datetime
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.translation import activate
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from api.accounts.models import MyUser
from api.events.models import Event
from api.tournaments.models import Tournament
from api.games.models import Game

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

token_regex = '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'


class Games(TestCase):
    token = None
    user = None
    event = None
    other_event = None
    tournament = None
    other_tournament = None
    game = None
    next_game = None
    game_other_day = None
    game_other_event = None

    def setUp(self):
        self.user = MyUser.objects.create_user(email='test@byom.de',
                                               first_name='Test',
                                               last_name='User',
                                               phone='+49192481024')
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()
        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)

        self.event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        self.other_event = Event.objects.create(
            name='Other Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        self.tournament = Tournament.objects \
            .create(name='Test Turnier',
                    gender='mixed',
                    deadline_signup='2017-01-01T00:00:00Z',
                    deadline_edit='2017-01-01T00:00:00Z',
                    advertisement_url='http://www.google.de',
                    contact_email='test@byom.de',
                    starting_fee=60.0,
                    number_of_places=12,
                    event=self.event
                    )
        self.other_tournament = Tournament.objects \
            .create(name='Test Anderes Turnier',
                    gender='mixed',
                    deadline_signup='2017-01-01T00:00:00Z',
                    deadline_edit='2017-01-01T00:00:00Z',
                    advertisement_url='http://www.google.de',
                    contact_email='test@byom.de',
                    starting_fee=60.0,
                    number_of_places=12,
                    event=self.other_event
                    )
        self.game = Game.objects.create(
            number=1,
            datetime='2017-01-01T10:00:00',
            event=self.event,
            court=1,
        )
        self.next_game = Game.objects.create(
            number=2,
            datetime='2017-01-01T11:00:00',
            event=self.event,
            court=1,
        )
        self.game_other_day = Game.objects.create(
            number=3,
            datetime='2017-01-02T10:00:00',
            event=self.event,
            court=1,
        )
        self.game_other_event = Game.objects.create(
            number=2,
            datetime='2017-01-01T10:00:00',
            event=self.other_event,
            court=1,
        )

    def test_game_set_delay(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                               kwargs={'pk': self.game.id}), {
            'delay': 35
        })
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['datetime'], '2017-01-01T10:35:00')
        self.assertEqual(data[1]['datetime'], '2017-01-01T11:35:00')
        game = Game.objects.get(pk=self.game.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 1, 10, 35, 0))

    def test_game_set_negative_delay(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                               kwargs={'pk': self.game.id}), {
            'delay': -10
        })
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['datetime'], '2017-01-01T09:50:00')
        self.assertEqual(data[1]['datetime'], '2017-01-01T10:50:00')
        game = Game.objects.get(pk=self.game.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 1, 9, 50, 0))

    def test_game_set_delay_next_game(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                                       kwargs={'pk': self.game.id}), {
                                   'delay': 35
                               })
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['datetime'], '2017-01-01T10:35:00')
        self.assertEqual(data[1]['datetime'], '2017-01-01T11:35:00')
        game = Game.objects.get(pk=self.next_game.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 1, 11, 35, 0))

    def test_game_not_set_delay_other_day(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                                       kwargs={'pk': self.game.id}), {
                                   'delay': 35
                               })
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)
        game = Game.objects.get(pk=self.game_other_day.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 2, 10, 00, 0))

    def test_game_not_set_delay_other_event(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                                       kwargs={'pk': self.game.id}), {
                                   'delay': 35
                               })
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)
        game = Game.objects.get(pk=self.game_other_event.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 1, 10, 00, 0))

    def test_game_set_delay_invalid_data(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                                       kwargs={'pk': self.game.id}), {})
        self.assertEqual(response.status_code, 400)
        game = Game.objects.get(pk=self.game.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 1, 10, 00, 0))

    def test_game_set_delay_unauthorized(self):
        client = APIClient()
        response = client.post(reverse('v1:game-delay',
                               kwargs={'pk': self.game.id}), {
                                   'delay': 35
                               })
        self.assertEqual(response.status_code, 401)

    def test_game_set_delay_wrong_method(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:game-delay',
                                      kwargs={'pk': self.game.id}), {
                                   'delay': 35
                               })
        self.assertEqual(response.status_code, 405)

    def test_game_set_delay_game_not_found(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-delay',
                                       kwargs={'pk': 99999999999}), {
                                   'delay': 35
                               })
        self.assertEqual(response.status_code, 404)

    def test_game_set_delay_not_admin_user(self):
        user = MyUser.objects.create(email='test2@byom.de',
                                     first_name='ANother',
                                     last_name='User',
                                     phone='+49192481024')
        user.set_password('test123')
        user.is_verified = True
        user.is_staff = False
        user.save()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        response = client.post(reverse('v1:game-delay',
                                       kwargs={'pk': self.game.id}), {
                                   'delay': 35
                               })
        self.assertEqual(response.status_code, 403)
        game = Game.objects.get(pk=self.game.id)
        self.assertEqual(game.datetime, datetime.datetime(2017, 1, 1, 10, 00, 0))
