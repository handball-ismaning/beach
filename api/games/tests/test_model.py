from datetime import timedelta
from django.test import TestCase
from django.utils.translation import activate

from api.accounts.models import MyUser
from api.enums import GameModeTypes
from api.events.models import Event
from api.games.models import Game
from api.groups.models import Group
from api.team.models import Team
from api.teamreferences.models import TeamReference
from api.tournaments.models import Tournament

activate('en-us')


class Games(TestCase):
    game = None
    event = None
    tournament = None
    group = None
    teamA = None
    teamB = None
    user = None

    def setUp(self):
        self.event = Event.objects \
            .create(name='Test Turnier',
                    start_date='2017-01-01',
                    end_date='2017-01-02',
                    start_time='09:00',
                    end_time='17:00',
                    game_duration=timedelta(minutes=12),
                    pause_duration=timedelta(minutes=3),
                    court_count=3
                    )
        self.tournament = Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=self.event
        )
        self.group = Group.objects.create(
            name='Test Gruppe',
            tournament=self.tournament
        )

        self.user = MyUser.objects.create(email='test@byom.de',
                                          first_name='Test',
                                          last_name='User',
                                          phone='+49192481024')
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

        self.teamA = Team.objects.create(
            name='Team A',
            tournament=self.tournament,
            trainer=self.user,
        )
        teamAReference = TeamReference.objects.create(
            team=self.teamA,
            group=self.group
        )
        teamBReference = TeamReference.objects.create(
            team=self.teamB,
            group=self.group
        )
        self.teamB = Team.objects.create(
            name='Team B',
            tournament=self.tournament,
            trainer=self.user,
        )

        self.game = Game.objects.create(
            number=1,
            group=self.group,
            teamA=teamAReference,
            teamB=teamBReference,
            event=self.event,
            court=1,
            datetime='2017-01-01T00:00:00Z'
        )

    def test_is_played(self):
        # Not played
        self.assertFalse(self.game.is_played)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 2
        self.game.save()

        # Player A won Halftime 1
        self.assertFalse(self.game.is_played)

        self.game.scoreAHalftime2 = 5
        self.game.scoreBHalftime2 = 0
        self.game.save()

        # Player A won Halftime 1 & 2
        self.assertTrue(self.game.is_played)

        self.game.scoreAHalftime2 = 1
        self.game.scoreBHalftime2 = 5
        self.game.save()

        # Player A won Halftime 1, Player B won Halftime 2
        self.assertFalse(self.game.is_played)

        self.game.scoreAHalftime3 = 2
        self.game.scoreBHalftime3 = 5
        self.game.save()

        # Player A won Halftime 1, Player B won Halftime 2 & 3
        self.assertTrue(self.game.is_played)

        # Simple mode
        self.game.scoreAHalftime1 = None
        self.game.scoreBHalftime1 = None
        self.game.scoreAHalftime2 = None
        self.game.scoreBHalftime2 = None
        self.game.scoreAHalftime3 = None
        self.game.scoreBHalftime3 = None
        self.event.game_mode = GameModeTypes.simple
        del self.game.is_simple_gamemode
        self.game.save()

        # Not played
        self.assertFalse(self.game.is_played)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 2
        self.game.save()

        # Player A won Halftime 1
        self.assertTrue(self.game.is_played)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 3
        self.game.save()

        # Draw in Halftime 1
        self.assertTrue(self.game.is_played)

    def test_pointsA(self):
        # Not played
        self.assertEqual(self.game.points_a, 0)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 2
        self.game.save()

        # Player A won Halftime 1
        self.assertEqual(self.game.points_a, 0)

        self.game.scoreAHalftime2 = 5
        self.game.scoreBHalftime2 = 0
        self.game.save()

        # Player A won Halftime 1 & 2
        self.assertEqual(self.game.points_a, 2)

        self.game.scoreAHalftime2 = 1
        self.game.scoreBHalftime2 = 5
        self.game.save()

        # Player A won Halftime 1, Player B won Halftime 2
        self.assertEqual(self.game.points_a, 0)

        self.game.scoreAHalftime3 = 2
        self.game.scoreBHalftime3 = 5
        self.game.save()

        # Player A won Halftime 1, Player B won Halftime 2 & 3
        self.assertEqual(self.game.points_a, 0)

        self.game.scoreAHalftime3 = 8
        self.game.scoreBHalftime3 = 5
        self.game.save()

        # Player A won Halftime 1 & 3, Player B won Halftime 2
        self.assertEqual(self.game.points_a, 2)

        # Simple mode
        self.game.scoreAHalftime1 = None
        self.game.scoreBHalftime1 = None
        self.game.scoreAHalftime2 = None
        self.game.scoreBHalftime2 = None
        self.game.scoreAHalftime3 = None
        self.game.scoreBHalftime3 = None
        self.event.game_mode = GameModeTypes.simple
        self.game.save()
        del self.game.is_simple_gamemode

        # Not playe
        self.assertEqual(self.game.points_a, 0)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 2
        self.game.save()

        # Player A won Halftime 1
        self.assertEqual(self.game.points_a, 2)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 3
        self.game.save()
        # Draw in Halftime 1

        self.assertEqual(self.game.points_a, 1)

    def test_pointsB(self):
        # Not played
        self.assertEqual(self.game.points_b, 0)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 2
        self.game.save()

        # Player A won Halftime 1
        self.assertEqual(self.game.points_b, 0)

        self.game.scoreAHalftime2 = 5
        self.game.scoreBHalftime2 = 0
        self.game.save()

        # Player A won Halftime 1 & 2
        self.assertEqual(self.game.points_b, 0)

        self.game.scoreAHalftime2 = 1
        self.game.scoreBHalftime2 = 5
        self.game.save()

        # Player A won Halftime 1, Player B won Halftime 2
        self.assertEqual(self.game.points_b, 0)

        self.game.scoreAHalftime3 = 2
        self.game.scoreBHalftime3 = 5
        self.game.save()

        # Player A won Halftime 1, Player B won Halftime 2 & 3
        self.assertEqual(self.game.points_b, 2)

        self.game.scoreAHalftime3 = 8
        self.game.scoreBHalftime3 = 5
        self.game.save()

        # Player A won Halftime 1 & 3, Player B won Halftime 2
        self.assertEqual(self.game.points_b, 0)

        # Simple mode
        self.game.scoreAHalftime1 = None
        self.game.scoreBHalftime1 = None
        self.game.scoreAHalftime2 = None
        self.game.scoreBHalftime2 = None
        self.game.scoreAHalftime3 = None
        self.game.scoreBHalftime3 = None
        self.event.game_mode = GameModeTypes.simple
        self.game.save()
        del self.game.is_simple_gamemode

        # Not played
        self.assertEqual(self.game.points_b, 0)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 2
        self.game.save()

        # Player A won Halftime 1
        self.assertEqual(self.game.points_b, 0)

        self.game.scoreAHalftime1 = 3
        self.game.scoreBHalftime1 = 3
        self.game.save()

        # Draw in Halftime 1
        self.assertEqual(self.game.points_b, 1)
