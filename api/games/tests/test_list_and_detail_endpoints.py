import json

from django.test import TransactionTestCase
from django.utils.translation import activate
from rest_framework_jwt.settings import api_settings
from rest_framework.test import APIClient
from django.core.urlresolvers import reverse

from api.accounts.models import MyUser
from api.events.models import Event
from api.team.models import Team
from api.teamreferences.models import TeamReference
from api.groups.models import Group
from api.tournaments.models import Tournament
from api.games.models import Game

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

token_regex = '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'


class Games(TransactionTestCase):
    group = None
    teamReference = None
    other_teamReference = None
    user = None
    team = None
    token = None
    tournament = None
    other_group = None
    game = None

    def setUp(self):
        event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        self.tournament = Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=event
        )
        self.group = Group.objects.create(
            name='Test Gruppe',
            tournament=self.tournament
        )
        self.other_group = Group.objects.create(
            name='Andere Gruppe',
            tournament=self.tournament
        )
        self.user = MyUser.objects.create(
            email='test@byom.de',
            first_name='Test',
            last_name='User',
            phone='+49192481024'
        )
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)

        self.team = Team.objects.create(
            name='TSV Ismaning',
            beachname='THC Eh Drin!',
            tournament=self.tournament,
            trainer=self.user,
        )
        self.teamReference = TeamReference.objects.create(
            team=self.team,
            group=self.group
        )
        self.other_teamReference = TeamReference.objects.create(
            from_group=self.other_group,
            group=self.group,
            place=2
        )
        self.game = Game.objects.create(
            number=1,
            datetime='2017-01-01T00:00:00Z',
            group=self.group,
            event=event,
            court=1,
            teamA=self.teamReference,
            teamB=self.other_teamReference
        )

    def test_game_list_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:game-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['number'], 1)
        self.assertGreaterEqual(data[0]['id'], 1)

    def test_game_list_get_unauthorized(self):
        client = APIClient()
        response = client.get(reverse('v1:game-list'))
        self.assertNotEqual(response.status_code, 401)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['number'], 1)
        self.assertGreaterEqual(data[0]['id'], 1)

    def test_game_list_get_invalid_token(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT absolute_invalid_token')
        response = client.get(reverse('v1:game-list'))
        self.assertEqual(response.status_code, 401)

    def test_game_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:game-detail',
                                      kwargs={'pk': self.game.id}))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['number'], 1)
        self.assertGreaterEqual(data['id'], 1)

    def test_game_get_unkown(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:game-detail',
                                      kwargs={'pk': 9999999999}))
        self.assertEqual(response.status_code, 404)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'], 'Not found.')

    def test_game_post(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:game-list'), {})
        self.assertEqual(response.status_code, 405)

    def test_game_put(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.put(reverse('v1:game-detail',
                                      kwargs={'pk': self.game.id}), {})
        self.assertEqual(response.status_code, 405)

    def test_game_patch(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 2,
                                    'scoreAHalftime2': 5,
                                    'scoreBHalftime2': 0
                                })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['number'], 1)
        self.assertTrue(data['is_played'])
        self.assertEqual(data['points_a'], 2)
        self.assertEqual(data['points_b'], 0)

    def test_game_delete(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.delete(reverse('v1:game-detail',
                                         kwargs={'pk': self.game.id}))
        self.assertEqual(response.status_code, 405)

    def test_game_patch_not_complete_ht_1(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': None,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_not_complete_ht_2(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': None,
                                    'scoreBHalftime2': 3,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_not_complete_ht_3(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': 1,
                                    'scoreBHalftime2': 3,
                                    'scoreAHalftime3': 1,
                                    'scoreBHalftime3': None,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_only_ht_3(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': None,
                                    'scoreBHalftime1': None,
                                    'scoreAHalftime2': None,
                                    'scoreBHalftime2': None,
                                    'scoreAHalftime3': 1,
                                    'scoreBHalftime3': 5,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_only_ht_2(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': None,
                                    'scoreBHalftime1': None,
                                    'scoreAHalftime2': 1,
                                    'scoreBHalftime2': 5,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_draw_ht_1(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 3,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': 1,
                                    'scoreBHalftime2': 3,
                                    'scoreAHalftime3': 1,
                                    'scoreBHalftime3': 2,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_draw_ht_2(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': 3,
                                    'scoreBHalftime2': 3,
                                    'scoreAHalftime3': 1,
                                    'scoreBHalftime3': 2,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_draw_ht_3(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': 3,
                                    'scoreBHalftime2': 6,
                                    'scoreAHalftime3': 2,
                                    'scoreBHalftime3': 2,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_penalty_though_won(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': 8,
                                    'scoreBHalftime2': 3,
                                    'scoreAHalftime3': 4,
                                    'scoreBHalftime3': 2,
                                })
        self.assertEqual(response.status_code, 400)

    def test_game_patch_valid_with_zeroes(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.patch(reverse('v1:game-detail',
                                        kwargs={'pk': self.game.id}), {
                                    'scoreAHalftime1': 4,
                                    'scoreBHalftime1': 3,
                                    'scoreAHalftime2': 0,
                                    'scoreBHalftime2': 3,
                                    'scoreAHalftime3': 0,
                                    'scoreBHalftime3': 2,
                                })
        self.assertEqual(response.status_code, 200)

    def test_game_set_referees(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.put(reverse('v1:game-referees',
                                      kwargs={'pk': self.game.id}), {
                                  'referees': 'Tom Jerry',
                              })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['referees'], 'Tom Jerry')

    def test_game_set_referees_not_admin(self):
        user = MyUser.objects.create(email='test2@byom.de',
                                     first_name='ANother',
                                     last_name='User',
                                     phone='+49192481024')
        user.set_password('test123')
        user.is_verified = True
        user.is_staff = False
        user.save()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        response = client.put(reverse('v1:game-referees',
                                      kwargs={'pk': self.game.id}), {
                                  'referees': 'Tom Jerry',
                              })
        self.assertEqual(response.status_code, 403)
