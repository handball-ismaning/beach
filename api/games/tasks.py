# Create your tasks here
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from celery.utils.log import get_task_logger
from api.games.models import Game

logger = get_task_logger(__name__)


@shared_task(bind=True, max_retries=2, default_retry_delay=30)
def update_team_and_group(self, game_id):
    logger.debug("Start Updating Team And Group of Game: " + str(game_id))
    try:
        game = Game.objects.get(pk=game_id)
        game.teamA.save()
        game.teamB.save()
        game.group.save()
        logger.debug("Finished Updating Team And Group of Game: " + str(game_id))
    except Game.DoesNotExist as exc:
        self.retry(exc=exc, throw=False)
    except Exception as exc:
        raise self.retry(exc=exc)
