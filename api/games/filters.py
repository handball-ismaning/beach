from django.db.models import Q
from django_filters import rest_framework as filters

from .models import Game


class GameFilter(filters.FilterSet):
    team = filters.NumberFilter(method='filter_team')

    def filter_team(self, queryset, name, value):
        return queryset.filter(Q(teamA__resolved_team__id=value) | Q(teamB__resolved_team__id=value))

    class Meta:
        model = Game
        fields = {
            'id': ['exact'],
            'number': ['exact', 'range'], 'event': ['exact'], 'group': ['exact'], 'group__tournament': ['exact'],
            'teamA': ['exact'], 'teamB': ['exact'], 'court': ['exact'], 'datetime': ['exact', 'date', 'time']
        }
