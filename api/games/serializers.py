from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin

from api.games.models import Game


class GameSerializer(SerializerExtensionsMixin, serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('id', 'number', 'datetime', 'court', 'is_played', 'is_empty',
                  'scoreAHalftime1', 'scoreBHalftime1', 'scoreAHalftime2', 'scoreBHalftime2',
                  'scoreAHalftime3', 'scoreBHalftime3', 'points_a', 'points_b', 'sets_a', 'sets_b',
                  'referees')
        read_only_fields = ('id', 'is_played', 'points_a', 'points_b', 'sets_a', 'sets_b')
        expandable_fields = dict(
            group='api.groups.serializers.GroupSerializer',
            teamA='api.teamreferences.serializers.TeamReferenceSerializer',
            teamB='api.teamreferences.serializers.TeamReferenceSerializer',
            event='api.events.serializers.EventSerializer',
        )

    def validate(self, data):
        scoreAHalftime1 = data.get('scoreAHalftime1', None)
        scoreBHalftime1 = data.get('scoreBHalftime1', None)
        scoreAHalftime2 = data.get('scoreAHalftime2', None)
        scoreBHalftime2 = data.get('scoreBHalftime2', None)
        scoreAHalftime3 = data.get('scoreAHalftime3', None)
        scoreBHalftime3 = data.get('scoreBHalftime3', None)
        has_ht_1 = (scoreAHalftime1 is not None and scoreBHalftime1 is not None)
        has_ht_2 = (scoreAHalftime2 is not None and scoreBHalftime2 is not None)
        has_ht_3 = (scoreAHalftime3 is not None and scoreBHalftime3 is not None)
        has_empty_ht_1 = (scoreAHalftime1 is None and scoreBHalftime1 is None)
        has_empty_ht_2 = (scoreAHalftime2 is None and scoreBHalftime2 is None)
        has_empty_ht_3 = (scoreAHalftime3 is None and scoreBHalftime3 is None)

        if not has_empty_ht_3 and not has_ht_3:
            raise serializers.ValidationError(_("Halftime 3 needs both teams to be filled"))

        if not has_empty_ht_2 and not has_ht_2:
            raise serializers.ValidationError(_("Halftime 2 needs both teams to be filled"))

        if not has_empty_ht_1 and not has_ht_1:
            raise serializers.ValidationError(_("Halftime 1 needs both teams to be filled"))

        if has_ht_2 and not has_ht_1:
            raise serializers.ValidationError(_("Halftime 2 needs first halftime to be filled"))

        if has_ht_3:
            if not has_ht_1 or not has_ht_2:
                raise serializers.ValidationError(_("Halftime 3 needs both other halftimes to be filled"))

        if has_ht_1 and has_ht_2:
            winner_1 = 'A' if scoreAHalftime1 > scoreBHalftime1 else 'B'
            winner_2 = 'A' if scoreAHalftime2 > scoreBHalftime2 else 'B'

            if scoreAHalftime1 == scoreBHalftime1:
                raise serializers.ValidationError(_("Halftime 1 cannot be a tie"))

            if scoreAHalftime2 == scoreBHalftime2:
                raise serializers.ValidationError(_("Halftime 2 cannot be a tie"))

            if has_ht_3:
                if winner_1 == winner_2:
                    raise serializers.ValidationError(_("Halftime 3 cannot be filled if game is already decided"))

                if scoreAHalftime3 == scoreBHalftime3:
                    raise serializers.ValidationError(_("Halftime 3 cannot be a tie"))

        data.scoreAHalftime1 = scoreAHalftime1
        data.scoreBHalftime1 = scoreBHalftime1
        data.scoreAHalftime2 = scoreAHalftime2
        data.scoreBHalftime2 = scoreBHalftime2
        data.scoreAHalftime3 = scoreAHalftime3
        data.scoreBHalftime3 = scoreBHalftime3
        return data
