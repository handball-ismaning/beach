from datetime import timedelta
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin
from django.utils.translation import gettext_lazy as _

from api.games.serializers import GameSerializer
from api.permissions import IsAdminOrReadOnly
from .filters import GameFilter
from .models import Game


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    return False


class GameViewSet(SerializerExtensionsAPIViewMixin, viewsets.ModelViewSet):
    queryset = Game.objects\
        .select_related('teamA', 'teamB', 'group', 'event', 'group__tournament')\
        .all().order_by('number')
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = GameSerializer
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_class = GameFilter
    extensions_expand_id_only = {}

    @detail_route(methods=['put', 'patch'], permission_classes=[IsAdminOrReadOnly])
    def update(self, request, pk=None, **kwargs):
        data = {
            'scoreAHalftime1': request.data.get('scoreAHalftime1', None),
            'scoreBHalftime1': request.data.get('scoreBHalftime1', None),
            'scoreAHalftime2': request.data.get('scoreAHalftime2', None),
            'scoreBHalftime2': request.data.get('scoreBHalftime2', None),
            'scoreAHalftime3': request.data.get('scoreAHalftime3', None),
            'scoreBHalftime3': request.data.get('scoreBHalftime3', None),
        }
        game = get_object_or_404(Game.objects.all(), pk=pk)
        self.check_object_permissions(self.request, game)

        serializer = GameSerializer(game, data=data, partial=True, context={'request': request, 'game_id': pk})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'], permission_classes=[IsAdminUser])
    def set_delay(self, request, pk=None, **kwargs):
        if not request.auth:
            return Response({'detail': _('Authentication credentials were not provided.')},
                            status=status.HTTP_401_UNAUTHORIZED)

        if not request.user.is_staff:
            return Response({'detail': _('You do not have permission to perform this action.')},
                            status=status.HTTP_403_FORBIDDEN)

        game = get_object_or_404(Game.objects.all(), pk=pk)

        delay = request.data.get('delay', None)
        if not delay or not is_number(delay):
            return Response({'detail': _('You did not provide a correct delay duration in minutes.')},
                            status=status.HTTP_400_BAD_REQUEST)
        d = timedelta(minutes=int(delay))

        games = Game.objects.all()\
            .filter(event=game.event)\
            .filter(number__gte=game.number)\
            .filter(datetime__date=game.datetime).all()

        for g in games:
            g.datetime = g.datetime + d
            g.save()

        return Response(GameSerializer(games, many=True).data, status=status.HTTP_200_OK)

    @detail_route(methods=['put'], permission_classes=[IsAdminUser])
    def update_referees(self, request, pk=None, **kwargs):
        data = {
            'referees': request.data.get('referees', None),
        }
        game = get_object_or_404(Game.objects.all(), pk=pk)
        self.check_object_permissions(self.request, game)

        serializer = GameSerializer(game, data=data, partial=True, context={'request': request, 'game_id': pk})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
