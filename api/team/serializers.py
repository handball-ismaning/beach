from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin

from api.accounts.serializers import UserSerializer
from api.players.models import Player
from api.players.serializers import PlayerSerializer
from api.team.models import Team


class TeamSerializer(SerializerExtensionsMixin, serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ('id', 'name', 'beachname', 'date_signup', 'state', 'paid',
                  'is_displayed', 'complete_name', 'has_players', 'players',
                  'trainer_name', 'trainer_phone', 'trainer_email')
        read_only_fields = ('id', 'is_displayed', 'complete_name', 'has_players', 'players')
        expandable_fields = dict(
            tournament=dict(
                serializer='api.tournaments.serializers.TournamentSerializer',
                read_only=False
            ),
            players=serializers.SerializerMethodField,
            trainer=serializers.SerializerMethodField
        )

    def validate(self, data):
        try:
            tournament = data.get('tournament_id') \
                if not self.instance else self.instance.tournament
        except ObjectDoesNotExist:
            raise serializers.ValidationError(
                _('Tournament not Found')
            )

        unique_error = serializers.ValidationError({
            'detail': _('Name already taken'),
            'key': 'name_already_taken'
        })
        for team in tournament.teams.all().exclude(id=self.context.get('team_id')):
            if data.get('beachname') is None:
                if team.name == data.get('name'):
                    raise unique_error
            else:
                if team.name == data.get('name') and team.beachname == data.get('beachname'):
                    raise unique_error

        number_list = []
        name_list = []
        players = data.get('players', self.context.get('players', None))
        if players is not None:
            for player in players:
                number_list.append(player.get('number'))
                name_list.append(player.get('first_name') + '-' + player.get('last_name'))

            if len(list(set(number_list))) != len(number_list):
                raise serializers.ValidationError({
                    'detail': _('Duplicate Player Number'),
                    'key': 'duplicate_player_number'
                })

            if len(list(set(name_list))) != len(name_list):
                raise serializers.ValidationError({
                    'detail': _('Duplicate Player Name'),
                    'key': 'duplicate_player_name'
                })

        trainer_data_none_count = 0
        if data.get('trainer_name') is not None:
            trainer_data_none_count += 1
        if data.get('trainer_email') is not None:
            trainer_data_none_count += 1
        if data.get('trainer_phone') is not None:
            trainer_data_none_count += 1

        if 0 < trainer_data_none_count < 3:
            raise serializers.ValidationError({
                'detail': _('Invalid Trainer Data'),
                'key': 'trainer_data_invalid'
            })

        return data

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.beachname = validated_data.get('beachname', instance.beachname)
        instance.state = validated_data.get('state', instance.state)
        instance.paid = validated_data.get('paid', instance.paid)
        instance.trainer_name = validated_data.get('trainer_name', instance.trainer_name)
        instance.trainer_email = validated_data.get('trainer_email', instance.trainer_email)
        instance.trainer_phone = validated_data.get('trainer_phone', instance.trainer_phone)

        players = validated_data.get('players', self.context.get('players', None))
        if players is not None:
            Player.objects.filter(team=instance).delete()
            for player in players:
                Player.objects.create(team=instance, **player)

        instance.save()
        return instance

    def create(self, validated_data):
        tournament = validated_data.get('tournament_id_resolved')
        trainer = self.context['request'].user
        validated_data.pop('tournament_id_resolved')

        obj = Team.objects.create(trainer=trainer,
                                  tournament=tournament,
                                  **validated_data)
        obj.save()
        return obj

    def get_trainer(self, obj):
        request = self.context['request']
        if request.user and (request.user.is_staff or request.user == obj.trainer):
            return self.represent_child(
                name='trainer',
                serializer=UserSerializer,
                instance=obj.trainer
            )

        return {}

    def get_players(self, obj):
        request = self.context['request']
        if request.user and (request.user.is_staff or request.user == obj.trainer):
            return PlayerSerializer(obj.players.all(), many=True).data

        return []
