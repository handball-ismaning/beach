from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TeamreferencesConfig(AppConfig):
    name = 'api.teamreferences'
    verbose_name = _('Team Reference Administration')

    def ready(self):
        import api.teamreferences.signals  # noqa
