from django.conf.urls import url

from .views import TeamReferenceViewSet

teamreference_list = TeamReferenceViewSet.as_view({
    'get': 'list'
})
teamreference_detail = TeamReferenceViewSet.as_view({
    'get': 'retrieve',
})

urlpatterns = [
    url(r'^$', teamreference_list, name='teamreference-list'),
    url(r'^(?P<pk>[0-9]+)/$', teamreference_detail, name='teamreference-detail'),
]
