# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-08-05 11:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teamreferences', '0004_teamreference_did_win_against'),
    ]

    operations = [
        migrations.AddField(
            model_name='teamreference',
            name='sets',
            field=models.PositiveIntegerField(default=0, help_text='Sets won by the team', verbose_name='Sets'),
        ),
        migrations.AddField(
            model_name='teamreference',
            name='sets_diff',
            field=models.IntegerField(default=0, help_text='Set difference by the team', verbose_name='Set difference'),
        ),
        migrations.AddField(
            model_name='teamreference',
            name='sets_negative',
            field=models.PositiveIntegerField(default=0, help_text='Sets list by the team', verbose_name='Sets negative'),
        ),
    ]
