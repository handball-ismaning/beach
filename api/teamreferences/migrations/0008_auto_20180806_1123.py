# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-08-06 09:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teamreferences', '0007_auto_20180805_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='teamreference',
            name='goals',
            field=models.PositiveIntegerField(default=0, help_text='Goals shot by the team', verbose_name='Goals'),
        ),
        migrations.AlterField(
            model_name='teamreference',
            name='did_win_against',
            field=models.ManyToManyField(blank=True, related_name='did_loose_against', to='teamreferences.TeamReference'),
        ),
    ]
