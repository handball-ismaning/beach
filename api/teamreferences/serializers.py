from rest_framework import serializers
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin

from api.teamreferences.models import TeamReference


class TeamReferenceSerializer(SerializerExtensionsMixin, serializers.ModelSerializer):
    class Meta:
        model = TeamReference
        fields = ('id', 'place', 'name', 'is_absolute', 'is_relative',
                  'is_resolved', 'points', 'points_negative', 'points_diff',
                  'sets', 'sets_negative', 'sets_diff', 'goals', 'goals_negative', 'goals_diff',
                  'games_count', 'games_played_count')
        read_only_fields = ('id', 'is_absolute', 'is_relative', 'name', 'is_resolved',
                            'points', 'points_negative', 'points_diff',
                            'sets', 'sets_negative', 'sets_diff', 'goals', 'goals_negative', 'goals_diff',
                            'games_count', 'games_played_count')
        expandable_fields = dict(
            group='api.groups.serializers.GroupSerializer',
            from_group='api.groups.serializers.GroupSerializer',
            team='api.team.serializers.TeamSerializer',
            resolved_team='api.team.serializers.TeamSerializer'
        )
