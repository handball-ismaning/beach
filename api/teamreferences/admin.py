from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from api.teamreferences.models import TeamReference


class TeamReferenceAdmin(admin.ModelAdmin):
    list_display = ('name', )
    fieldsets = (
        (_("General"), {'fields': ('group',)}),
        (_("Absolute"), {'fields': ('team',)}),
        (_("Relative"), {'fields': ('from_group', 'place')}),
        (_("Results"), {'fields': ('place_in_group', 'points', 'points_negative', 'sets',
                                   'sets_negative', 'goals', 'goals_negative')})
    )


admin.site.register(TeamReference, TeamReferenceAdmin)
