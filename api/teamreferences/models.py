from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Sum
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from api.team.models import Team
from api.groups.models import Group


class TeamReference(models.Model):
    team = models.ForeignKey(
        Team,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='references_team',
        db_index=True,
        help_text=_("direct team of the reference"), verbose_name=_("Reference Team")
    )
    from_group = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='references_group',
        db_index=True,
        help_text=_("group reference"), verbose_name=_("Reference Group")
    )
    group = models.ForeignKey(
        Group,
        related_name='teams',
        db_index=True,
        help_text=_("groups that this reference is used in"), verbose_name=_("Groups")
    )
    place = models.PositiveSmallIntegerField(help_text=_("The place the team has reached in the reference group"),
                                             verbose_name=_("Place"), default=1,
                                             validators=[MinValueValidator(1)])
    place_in_group = models.PositiveSmallIntegerField(
        help_text=_("The place the team has reached in its group"),
        verbose_name=_("Achieved Place"), default=1,
        validators=[MinValueValidator(1)]
    )
    resolved_team = models.ForeignKey(
        Team,
        blank=True,
        null=True,
        db_index=True,
        on_delete=models.CASCADE,
        related_name='references_resolved_team',
        help_text=_("resolved team of the reference"), verbose_name=_("Resolved Team")
    )
    points = models.PositiveIntegerField(
        help_text=_("Points won by the team"), verbose_name=_("Points"),
        default=0
    )
    points_negative = models.PositiveIntegerField(
        help_text=_("Points lost by the team"), verbose_name=_("Points negative"),
        default=0
    )
    points_diff = models.IntegerField(
        help_text=_("Points difference by the team"), verbose_name=_("Points difference"),
        default=0
    )
    sets = models.PositiveIntegerField(
        help_text=_("Sets won by the team"), verbose_name=_("Sets"),
        default=0
    )
    sets_negative = models.PositiveIntegerField(
        help_text=_("Sets lost by the team"), verbose_name=_("Sets negative"),
        default=0
    )
    sets_diff = models.IntegerField(
        help_text=_("Set difference by the team"), verbose_name=_("Set difference"),
        default=0
    )
    goals = models.PositiveIntegerField(
        help_text=_("Goals shot by the team"), verbose_name=_("Goals"),
        default=0
    )
    goals_negative = models.PositiveIntegerField(
        help_text=_("Goals against the team"), verbose_name=_("Goals negative"),
        default=0
    )
    goals_diff = models.IntegerField(
        help_text=_("Goal difference by the team"), verbose_name=_("Goal difference"),
        default=0
    )
    did_win_against = models.ManyToManyField(
        "self",
        symmetrical=False,
        related_name='did_loose_against',
        blank=True
    )
    games_count = models.PositiveIntegerField(
        help_text=_("Games that the team plays in"), verbose_name=_("Count Games"),
        default=0
    )
    games_played_count = models.PositiveIntegerField(
        help_text=_("Completed Games that the team plays in"), verbose_name=_("Count Played Games"),
        default=0
    )

    class Meta:
        verbose_name = _('Team Reference')
        verbose_name_plural = _('Team References')

    @cached_property
    def is_absolute(self):
        return hasattr(self, 'team') and self.team is not None

    @cached_property
    def is_relative(self):
        return hasattr(self, 'from_group') and self.from_group is not None

    @cached_property
    def is_resolved(self):
        return hasattr(self, 'resolved_team') and self.resolved_team is not None

    def clean(self):
        if not self.is_absolute and not self.is_relative:
            raise ValidationError(
                _('Either a Team or a Group needs to be referenced')
            )

        if self.is_absolute and self.is_relative:
            raise ValidationError(
                _('Cannot reference a team and group at the same time')
            )

        if self.is_relative and self.place > self.from_group.count_teams:
            raise ValidationError(
                _('Cannot reference place that is not available in the group')
            )

    @cached_property
    def name(self):
        if self.is_resolved:
            return self.resolved_team.complete_name

        if self.is_absolute:
            return self.team.complete_name

        return str(self.place) + '. ' + self.from_group.name

    def get_points(self):
        points_a = self.gamesA.all().aggregate(Sum('points_a')).get('points_a__sum', 0)
        points_b = self.gamesB.all().aggregate(Sum('points_b')).get('points_b__sum', 0)
        return (points_a if points_a is not None else 0) + (points_b if points_b is not None else 0)

    def get_points_negative(self):
        points_a = self.gamesA.all().aggregate(Sum('points_b')).get('points_b__sum', 0)
        points_b = self.gamesB.all().aggregate(Sum('points_a')).get('points_a__sum', 0)
        return (points_a if points_a is not None else 0) + (points_b if points_b is not None else 0)

    def get_sets(self):
        sets_a = self.gamesA.all().aggregate(Sum('sets_a')).get('sets_a__sum', 0)
        sets_b = self.gamesB.all().aggregate(Sum('sets_b')).get('sets_b__sum', 0)
        return (sets_a if sets_a is not None else 0) + (sets_b if sets_b is not None else 0)

    def get_sets_negative(self):
        sets_a = self.gamesA.all().aggregate(Sum('sets_b')).get('sets_b__sum', 0)
        sets_b = self.gamesB.all().aggregate(Sum('sets_a')).get('sets_a__sum', 0)
        return (sets_a if sets_a is not None else 0) + (sets_b if sets_b is not None else 0)

    def get_goals(self):
        goals_a = self.gamesA.all().aggregate(Sum('goals_a')).get('goals_a__sum', 0)
        goals_b = self.gamesB.all().aggregate(Sum('goals_b')).get('goals_b__sum', 0)
        return (goals_a if goals_a is not None else 0) + (goals_b if goals_b is not None else 0)

    def get_goals_negative(self):
        goals_a = self.gamesA.all().aggregate(Sum('goals_b')).get('goals_b__sum', 0)
        goals_b = self.gamesB.all().aggregate(Sum('goals_a')).get('goals_a__sum', 0)
        return (goals_a if goals_a is not None else 0) + (goals_b if goals_b is not None else 0)

    def get_games_count(self):
        return self.gamesA.count() + self.gamesB.count()

    def get_games_played_count(self):
        return self.gamesA.filter(group=self.group).filter(is_played=True).count() \
               + self.gamesB.filter(group=self.group).filter(is_played=True).count()

    def __str__(self):
        return self.name if self.is_resolved else self.name + ' - ' + str(self.from_group.tournament)
