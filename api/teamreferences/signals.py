from django.core.exceptions import ObjectDoesNotExist
from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import TeamReference


@receiver(pre_save, sender=TeamReference)
def pre_save_team_reference(sender, instance, **kwargs):
    instance.points = instance.get_points()
    instance.points_negative = instance.get_points_negative()
    instance.points_diff = instance.points - instance.points_negative

    instance.sets = instance.get_sets()
    instance.sets_negative = instance.get_sets_negative()
    instance.sets_diff = instance.sets - instance.sets_negative

    instance.goals = instance.get_goals()
    instance.goals_negative = instance.get_goals_negative()
    instance.goals_diff = instance.goals - instance.goals_negative

    instance.games_count = instance.get_games_count()
    instance.games_played_count = instance.get_games_played_count()

    teams_won = []
    for t in instance.gamesA.filter(winner=instance):
        teams_won.append(t.teamB)
    for t in instance.gamesB.filter(winner=instance):
        teams_won.append(t.teamA)

    if instance.id:
        instance.did_win_against.set(teams_won)

    if instance.team:
        instance.resolved_team = instance.team
    elif instance.from_group:
        if instance.from_group.is_completed:
            try:
                sorted_teams = list(instance.from_group.teams.all().order_by('place_in_group'))
                if instance.place > len(sorted_teams):
                    instance.resolved_team = None
                else:
                    instance.resolved_team = sorted_teams[instance.place - 1].resolved_team
            except ObjectDoesNotExist:
                instance.resolved_team = None
