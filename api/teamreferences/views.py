from rest_framework import viewsets
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin

from api.permissions import IsAdminOrReadOnly
from api.teamreferences.serializers import TeamReferenceSerializer
from .models import TeamReference


class TeamReferenceViewSet(SerializerExtensionsAPIViewMixin, viewsets.ModelViewSet):
    queryset = TeamReference.objects\
        .select_related('group', 'from_group', 'team', 'resolved_team')\
        .prefetch_related('gamesA', 'gamesB')\
        .all()
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = TeamReferenceSerializer
    pagination_class = None
