import json

from django.test import TransactionTestCase
from django.utils.translation import activate
from rest_framework_jwt.settings import api_settings
from rest_framework.test import APIClient
from django.core.urlresolvers import reverse

from api.accounts.models import MyUser
from api.events.models import Event
from api.team.models import Team
from api.teamreferences.models import TeamReference
from api.groups.models import Group
from api.tournaments.models import Tournament

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

token_regex = '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'


class TeamReferences(TransactionTestCase):
    group = None
    teamReference = None
    other_teamReference = None
    user = None
    team = None
    token = None
    tournament = None
    other_group = None

    def setUp(self):
        event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        self.tournament = Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=event
        )
        self.group = Group.objects.create(
            name='Test Gruppe',
            tournament=self.tournament
        )
        self.other_group = Group.objects.create(
            name='Andere Gruppe',
            tournament=self.tournament
        )
        self.user = MyUser.objects.create(
            email='test@byom.de',
            first_name='Test',
            last_name='User',
            phone='+49192481024'
        )
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)

        self.team = Team.objects.create(
            name='TSV Ismaning',
            beachname='THC Eh Drin!',
            tournament=self.tournament,
            trainer=self.user,
        )
        self.teamReference = TeamReference.objects.create(
            team=self.team,
            group=self.group
        )
        self.other_teamReference = TeamReference.objects.create(
            from_group=self.other_group,
            group=self.group,
            place=2
        )

    def test_teamReference_list_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:teamreference-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['name'], self.team.complete_name)
        self.assertGreaterEqual(data[0]['id'], 1)
        self.assertEqual(data[1]['name'], '2. ' + self.other_group.name)
        self.assertGreaterEqual(data[1]['id'], 2)

    def test_teamReference_list_get_unauthorized(self):
        client = APIClient()
        response = client.get(reverse('v1:teamreference-list'))
        self.assertNotEqual(response.status_code, 401)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['name'], self.team.complete_name)
        self.assertGreaterEqual(data[0]['id'], 1)
        self.assertEqual(data[1]['name'], '2. ' + self.other_group.name)
        self.assertGreaterEqual(data[1]['id'], 2)

    def test_teamReference_list_get_invalid_token(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT absolute_invalid_token')
        response = client.get(reverse('v1:teamreference-list'))
        self.assertEqual(response.status_code, 401)

    def test_teamReference_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:teamreference-detail',
                                      kwargs={'pk': self.teamReference.id}))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['name'], self.team.complete_name)
        self.assertGreaterEqual(data['id'], 1)

    def test_teamReference_get_unkown(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:teamreference-detail',
                                      kwargs={'pk': 9999999999}))
        self.assertEqual(response.status_code, 404)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'], 'Not found.')

    def test_teamreference_post(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:teamreference-list'), {})
        self.assertEqual(response.status_code, 405)

    def test_teamreference_put(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.put(reverse('v1:teamreference-detail',
                                      kwargs={'pk': self.teamReference.id}), {})
        self.assertEqual(response.status_code, 405)

    def test_teamreference_delete(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.delete(reverse('v1:teamreference-detail',
                                         kwargs={'pk': self.teamReference.id}))
        self.assertEqual(response.status_code, 405)
