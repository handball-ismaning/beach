from django.test import TestCase
from django.utils.translation import activate

from api.accounts.models import MyUser
from api.events.models import Event
from api.games.models import Game
from api.team.models import Team
from api.groups.models import Group
from api.teamreferences.models import TeamReference
from api.tournaments.models import Tournament

activate('en-us')


def set_game_score(game, ht1a, ht1b, ht2a, ht2b, ht3a, ht3b):
    game.scoreAHalftime1 = ht1a
    game.scoreBHalftime1 = ht1b
    game.scoreAHalftime2 = ht2a
    game.scoreBHalftime2 = ht2b
    game.scoreAHalftime3 = ht3a
    game.scoreBHalftime3 = ht3b
    game.save()


class TeamReferences(TestCase):
    teamReference = None
    user = None
    team = None
    group = None
    other_teamReference = None
    other_group = None

    def setUp(self):
        event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        tournament = Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=event
        )
        self.group = Group.objects.create(
            name='Test Gruppe',
            tournament=tournament
        )
        self.other_group = Group.objects.create(
            name='Gruppe A',
            tournament=tournament
        )
        self.new_group = Group.objects.create(
            name='Gruppe Neu',
            tournament=tournament
        )
        self.user = MyUser.objects.create(
            email='test@byom.de',
            first_name='Test',
            last_name='User',
            phone='+49192481024'
        )
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

        self.team = Team.objects.create(
            name='TSV Ismaning',
            beachname='THC Eh Drin!',
            tournament=tournament,
            trainer=self.user,
        )
        self.teamReference = TeamReference.objects.create(
            team=self.team,
            group=self.group
        )
        self.other_teamReference = TeamReference.objects.create(
            from_group=self.other_group,
            place=3,
            group=self.group
        )
        self.game = Game.objects.create(
            number=1,
            teamA=self.teamReference,
            teamB=self.other_teamReference,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:00:00Z'
        )
        self.referencing_team = TeamReference.objects.create(
            group=self.new_group,
            from_group=self.group,
            place=1
        )
        self.teamReference.refresh_from_db()

    def test_absolute_reference(self):
        self.assertTrue(self.teamReference.is_absolute)
        self.assertFalse(self.other_teamReference.is_absolute)

    def test_relative_reference(self):
        self.assertTrue(self.other_teamReference.is_relative)
        self.assertFalse(self.teamReference.is_relative)

    def test_name(self):
        self.assertEqual(self.teamReference.name, self.team.complete_name)
        self.assertEqual(self.other_teamReference.name, '3. ' + self.other_group.name)

    def test_games_count(self):
        self.assertEqual(self.teamReference.games_count, 1)

    def test_games_played_count(self):
        self.assertEqual(self.teamReference.games_played_count, 0)

        set_game_score(self.game, 5, 3, 6, 2, None, None)
        self.teamReference.refresh_from_db()

        self.assertEqual(self.teamReference.games_played_count, 1)

    def test_team_resolving(self):
        self.assertEqual(self.referencing_team.resolved_team, None)
        self.assertFalse(self.referencing_team.is_resolved)

        set_game_score(self.game, 5, 3, 6, 2, None, None)

        team_ref = TeamReference.objects.get(id=self.referencing_team.id)
        self.assertTrue(team_ref.is_resolved)
        self.assertEqual(team_ref.resolved_team, self.team)
