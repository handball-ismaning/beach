from rest_framework import serializers
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin

from api.groups.models import Group


class GroupSerializer(SerializerExtensionsMixin, serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name', 'count_teams', 'is_completed', 'level')
        read_only_fields = ('id', 'is_completed')
        expandable_fields = dict(
            tournament=dict(
                serializer='api.tournaments.serializers.TournamentSerializer',
                read_only=False
            ),
            teams=dict(
                serializer='api.teamreferences.serializers.TeamReferenceSerializer',
                many=True
            ),
            team_table=dict(
                serializer='api.teamreferences.serializers.TeamReferenceSerializer',
                many=True
            )
        )

    def create(self, validated_data):
        validated_data['tournament'] = validated_data['tournament_id_resolved']
        validated_data.pop('tournament_id_resolved')
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data.pop('tournament_id_resolved')
        return super().update(instance, validated_data)
