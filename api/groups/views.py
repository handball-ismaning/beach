from rest_framework import viewsets
from rest_framework.filters import DjangoFilterBackend
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin

from api.permissions import IsAdminOrReadOnly
from .models import Group
from .serializers import GroupSerializer
from .filters import GroupFilter


class GroupViewSet(SerializerExtensionsAPIViewMixin, viewsets.ModelViewSet):
    queryset = Group.objects.all()
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = GroupSerializer
    pagination_class = None
    extensions_expand_id_only = {'teams', 'team_table'}
    filter_backends = (DjangoFilterBackend,)
    filterset_class = GroupFilter

    def get_extensions_mixin_context(self):
        context = super(GroupViewSet, self).get_extensions_mixin_context()

        if self.request.method in ['POST', 'PUT']:
            context['expand'] = set()

        return context
