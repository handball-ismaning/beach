from django.contrib import admin

from api.teamreferences.models import TeamReference
from .models import Group


class TeamsInline(admin.TabularInline):
    model = TeamReference
    fk_name = 'group'
    extra = 0
    show_change_link = False
    fields = ('name', 'team', 'from_group', 'place', 'place_in_group', 'points', 'points_negative',
              'sets', 'sets_negative', 'goals', 'goals_negative')
    readonly_fields = ('name', 'points', 'points_negative', 'sets',
                       'sets_negative', 'goals', 'goals_negative')
    classes = ('collapse',)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'tournament', 'count_teams')
    fieldsets = (
        (None, {'fields': ('name', 'tournament', 'level')}),
    )
    inlines = [TeamsInline]


admin.site.register(Group, GroupAdmin)
