from django.test import TestCase
from django.utils.translation import activate

from api.accounts.models import MyUser
from api.events.models import Event
from api.games.models import Game
from api.team.models import Team
from api.groups.models import Group
from api.teamreferences.models import TeamReference
from api.tournaments.models import Tournament

activate('en-us')


def set_game_score(game, ht1a, ht1b, ht2a, ht2b, ht3a, ht3b):
    game.scoreAHalftime1 = ht1a
    game.scoreBHalftime1 = ht1b
    game.scoreAHalftime2 = ht2a
    game.scoreBHalftime2 = ht2b
    game.scoreAHalftime3 = ht3a
    game.scoreBHalftime3 = ht3b
    game.save()


class Groups(TestCase):
    group = None
    user = None
    game1 = None
    game2 = None
    game3 = None
    team1ref = None
    team2ref = None
    team3ref = None
    team4ref = None

    def setUp(self):
        event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        tournament = Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=event
        )
        self.group = Group.objects.create(
            name='Test Gruppe',
            tournament=tournament
        )
        self.user = MyUser.objects.create(
            email='test@byom.de',
            first_name='Test',
            last_name='User',
            phone='+49192481024'
        )
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

        team1 = Team.objects.create(
            name='Team 1',
            tournament=tournament,
            trainer=self.user,
        )
        team2 = Team.objects.create(
            name='Team 2',
            tournament=tournament,
            trainer=self.user,
        )
        team3 = Team.objects.create(
            name='Team 3',
            tournament=tournament,
            trainer=self.user,
        )
        team4 = Team.objects.create(
            name='Team 4',
            tournament=tournament,
            trainer=self.user,
        )
        self.team1ref = TeamReference.objects.create(
            team=team1,
            group=self.group
        )
        self.team2ref = TeamReference.objects.create(
            team=team2,
            group=self.group
        )
        self.team3ref = TeamReference.objects.create(
            team=team3,
            group=self.group
        )
        self.team4ref = TeamReference.objects.create(
            team=team4,
            group=self.group
        )

        self.game1 = Game.objects.create(
            number=1,
            teamA=self.team1ref,
            teamB=self.team2ref,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:00:00Z'
        )
        self.game2 = Game.objects.create(
            number=2,
            teamA=self.team1ref,
            teamB=self.team3ref,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:10:00Z'
        )
        self.game3 = Game.objects.create(
            number=3,
            teamA=self.team1ref,
            teamB=self.team4ref,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:20:00Z'
        )
        self.game4 = Game.objects.create(
            number=4,
            teamA=self.team2ref,
            teamB=self.team3ref,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:30:00Z'
        )
        self.game5 = Game.objects.create(
            number=5,
            teamA=self.team2ref,
            teamB=self.team4ref,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:40:00Z'
        )
        self.game6 = Game.objects.create(
            number=6,
            teamA=self.team3ref,
            teamB=self.team4ref,
            group=self.group,
            event=event,
            court=1,
            datetime='2017-01-01T00:50:00Z'
        )

    def test_count_teams(self):
        self.assertEqual(self.group.count_teams, 4)

    def test_team_table_no_games(self):
        for t in self.group.team_table:
            self.assertEqual(t.points, 0)
            self.assertTrue(t in [
                self.team1ref, self.team2ref, self.team3ref, self.team4ref
            ])

    def test_team_table_not_done_yet(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 8, 7, 9, None, None)  # 1 - 3
        self.assertTrue(list(self.group.team_table), [
            self.team3ref, self.team1ref, self.team2ref, self.team4ref
        ])

    def test_team_table_points(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 8, 7, 9, None, None)  # 1 - 3
        set_game_score(self.game3, 5, 4, 7, 3, None, None)  # 1 - 4
        set_game_score(self.game4, 5, 8, 7, 9, None, None)  # 2 - 3
        set_game_score(self.game5, 5, 4, 7, 3, None, None)  # 2 - 4
        set_game_score(self.game6, 5, 4, 7, 3, None, None)  # 3 - 4

        # 1: 4, 2: 2, 3: 6, 4: 0
        self.assertEqual(list(self.group.team_table), [
            self.team3ref, self.team1ref, self.team2ref, self.team4ref
        ])
        self.assertEqual(self.group.team_table.first().points, 6)

    def test_team_table_direct_comparison(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 8, 7, 9, None, None)  # 1 - 3
        set_game_score(self.game3, 5, 4, 7, 3, None, None)  # 1 - 4
        set_game_score(self.game4, 5, 8, 7, 9, None, None)  # 2 - 3
        set_game_score(self.game5, 5, 4, 7, 3, None, None)  # 2 - 4
        set_game_score(self.game6, 5, 8, 7, 9, None, None)  # 3 - 4
        # 1: 4, 2: 2, 3: 4, 4: 2
        self.assertEqual(list(self.group.team_table), [
            self.team3ref, self.team1ref, self.team2ref, self.team4ref
        ])

    def test_team_table_direct_comparison_multiple_points(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 3, 7, 3, None, None)  # 1 - 3
        set_game_score(self.game3, 5, 4, 7, 3, None, None)  # 1 - 4
        set_game_score(self.game4, 5, 8, 7, 9, None, None)  # 2 - 3 = 3
        set_game_score(self.game5, 5, 4, 1, 3, 4, 2)  # 2 - 4 = 2
        set_game_score(self.game6, 5, 8, 7, 9, None, None)  # 3 - 4 = 4
        # 1: 4, 2: 2, 3: 2, 4: 2
        self.assertEqual(list(self.group.team_table), [
            self.team1ref, self.team4ref, self.team3ref, self.team2ref
        ])

    def test_team_table_direct_comparison_multiple_set_diff(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 3, 7, 3, None, None)  # 1 - 3
        set_game_score(self.game3, 5, 4, 7, 3, None, None)  # 1 - 4
        set_game_score(self.game4, 5, 8, 7, 9, None, None)  # 2 - 3 = 3
        set_game_score(self.game5, 5, 4, 1, 3, 4, 2)  # 2 - 4 = 2
        set_game_score(self.game6, 5, 8, 7, 9, None, None)  # 3 - 4 = 4
        # 1: 4, 2: 2, 3: 2, 4: 2
        self.assertEqual(list(self.group.team_table), [
            self.team1ref, self.team4ref, self.team3ref, self.team2ref
        ])

    def test_team_table_direct_comparison_multiple_goal_diff(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 3, 7, 3, None, None)  # 1 - 3
        set_game_score(self.game3, 5, 4, 7, 3, None, None)  # 1 - 4
        set_game_score(self.game4, 5, 8, 7, 9, None, None)  # 2 - 3 = 3
        set_game_score(self.game5, 5, 4, 100, 3, None, None)  # 2 - 4 = 2
        set_game_score(self.game6, 5, 8, 7, 9, None, None)  # 3 - 4 = 4
        # 1: 4, 2: 2, 3: 2, 4: 2
        self.assertEqual(list(self.group.team_table), [
            self.team1ref, self.team2ref, self.team3ref, self.team4ref
        ])

    def test_team_table_direct_comparison_multiple_goals(self):
        set_game_score(self.game1, 5, 4, 7, 3, None, None)  # 1 - 2
        set_game_score(self.game2, 5, 3, 7, 3, None, None)  # 1 - 3
        set_game_score(self.game3, 5, 4, 7, 3, None, None)  # 1 - 4
        set_game_score(self.game4, 5, 8, 7, 16, None, None)  # 2 - 3 = 3
        set_game_score(self.game5, 5, 4, 27, 10, None, None)  # 2 - 4 = 2
        set_game_score(self.game6, 5, 8, 7, 9, None, None)  # 3 - 4 = 4
        # 1: 4, 2: 2, 3: 2, 4: 2
        self.assertEqual(list(self.group.team_table), [
            self.team1ref, self.team2ref, self.team3ref, self.team4ref
        ])
