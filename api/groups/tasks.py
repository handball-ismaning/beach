# Create your tasks here
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from celery.utils.log import get_task_logger
from api.groups.models import Group
from api.groups.teamSorter import TeamSorter
from api.teamreferences.models import TeamReference

logger = get_task_logger(__name__)


@shared_task(bind=True, max_retries=2, default_retry_delay=30)
def update_group_table(self, group_id):
    logger.debug("Start Updating Table of Group: " + str(group_id))
    try:
        group = Group.objects.get(pk=group_id)
        teams = list(group.teams.all().order_by('-points'))
        sorted_teams = TeamSorter(teams).sort()

        group.is_completed = group.get_is_completed()

        for index, team in enumerate(sorted_teams):
            TeamReference.objects.filter(id=team.id).update(place_in_group=(index + 1))

        for team_reference in group.references_group.all():
            team_reference.save()
        logger.debug("Finished Updating Table of Group: " + str(group))
    except (Group.DoesNotExist, TeamReference.DoesNotExist) as exc:
        self.retry(exc=exc, throw=False)
    except Exception as exc:
        raise self.retry(exc=exc)
