from django.conf.urls import url

from .views import GroupViewSet

group_list = GroupViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
group_detail = GroupViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    url(r'^$', group_list, name='group-list'),
    url(r'^(?P<pk>[0-9]+)/$', group_detail, name='group-detail'),
]
