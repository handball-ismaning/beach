from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GroupsConfig(AppConfig):
    name = 'api.groups'
    app_label = 'api_groups'
    verbose_name = _('Group Administration')

    def ready(self):
        import api.groups.signals  # noqa