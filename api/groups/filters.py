from django_filters import rest_framework as filters

from .models import Group


class GroupFilter(filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'id': ['exact'],
            'tournament': ['exact']
        }
