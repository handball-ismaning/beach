from django.db.models.signals import post_save
from django.dispatch import receiver

from api.groups.tasks import update_group_table
from .models import Group


@receiver(post_save, sender=Group)
def post_save_group(sender, instance, **kwargs):
    update_group_table.delay(instance.id)
