from operator import attrgetter
from django.utils.translation import gettext_lazy as _


class TeamSorter:
    initial_teams = []
    teams = []
    result = []

    def __init__(self, teams):
        self.initial_teams = teams
        self.teams = teams
        self.result = [None] * len(teams)

    def get_points_dict(self):
        result = {}
        for t in self.teams:
            current = result.get(t.points, []) or []
            current.append(t)
            result[t.points] = current
        return result

    def sort(self):
        # Sort by points
        self.teams = sorted(self.teams, key=attrgetter('points'), reverse=True)
        teams_by_points = self.get_points_dict()

        index = 0
        for t in self.teams:
            if t in self.result:
                continue

            competing_teams = teams_by_points.get(t.points, [])
            if len(competing_teams) == 1:
                self.result[index] = t
            elif len(competing_teams) == 2:
                a = competing_teams[0]
                b = competing_teams[1]
                if a.did_win_against.filter(id__exact=b.id).exists():
                    self.result[index] = a
                    self.result[index + 1] = b
                else:
                    self.result[index] = b
                    self.result[index + 1] = a
            else:
                sorted_current = sorted(
                    competing_teams,
                    key=attrgetter('sets_diff', 'goals_diff', 'goals'),
                    reverse=True
                )
                self.result[index:index + len(competing_teams)] = sorted_current

            index += len(competing_teams)

        if None in self.result:
            raise Exception(_("Team Sorting did fail"))

        return self.result
