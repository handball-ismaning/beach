from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from api.tournaments.models import Tournament


class Group(models.Model):
    name = models.CharField(max_length=120,
                            help_text=_("Name of the Group"),
                            verbose_name=_("Group Name"))
    tournament = models.ForeignKey(
        Tournament,
        on_delete=models.CASCADE,
        related_name='groups',
        db_index=True,
        help_text=_("tournament of the group"), verbose_name=_("Group tournament")
    )
    is_completed = models.BooleanField(
        default=False,
        help_text=_("all games of the group played"), verbose_name=_("Group completed")
    )
    level = models.PositiveIntegerField(
        default=1,
        help_text=_("level of the group within the group structure"), verbose_name=_("Group Level")
    )

    @cached_property
    def count_teams(self):
        return self.teams.count()

    @cached_property
    def team_table(self):
        return self.teams.order_by('place_in_group')

    def get_is_completed(self):
        return not self.games.filter(is_played=False).exists()

    class Meta:
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def __str__(self):
        return self.name + ' - ' + str(self.tournament)
