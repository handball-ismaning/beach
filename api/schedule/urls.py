from django.conf.urls import url

from .views import GroupStructureView

urlpatterns = [
    url(r'^group_structure/(?P<pk>[0-9]+)/$', GroupStructureView.as_view(), name='schedule-group-structure'),
]
