# Create your tasks here
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from celery.utils.log import get_task_logger
from api.games.models import Game

logger = get_task_logger(__name__)


@shared_task(bind=True, max_retries=2, default_retry_delay=30)
def save_games(self, game_ids):
    logger.warn("Start Saving Games")
    try:
        games = Game.objects.all().filter(id__in=game_ids)
        for game in games:
            game.save()
        logger.debug("Finished Saving Games")
    except Game.DoesNotExist as exc:
        self.retry(exc=exc, throw=False)
    except Exception as exc:
        raise self.retry(exc=exc)
