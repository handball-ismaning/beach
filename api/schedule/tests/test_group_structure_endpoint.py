import json

from django.core.urlresolvers import reverse
from django.test import TransactionTestCase
from django.utils.translation import activate
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from api.accounts.models import MyUser
from api.enums import TeamStateTypes
from api.events.models import Event
from api.games.models import Game
from api.groups.models import Group
from api.team.models import Team
from api.teamreferences.models import TeamReference
from api.tournaments.models import Tournament

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

token_regex = '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'


class GroupStructure(TransactionTestCase):
    event = None
    group = None
    teamReference = None
    other_teamReference = None
    user = None
    team = None
    token = None
    tournament = None
    other_group = None

    def setUp(self):
        self.event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        self.tournament = Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=self.event
        )
        self.group = Group.objects.create(
            name='Test Gruppe',
            tournament=self.tournament
        )
        self.other_group = Group.objects.create(
            name='Andere Gruppe',
            tournament=self.tournament
        )
        self.user = MyUser.objects.create(
            email='test@byom.de',
            first_name='Test',
            last_name='User',
            phone='+49192481024'
        )
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)

        self.team = Team.objects.create(
            name='TSV Ismaning',
            beachname='THC Eh Drin!',
            tournament=self.tournament,
            trainer=self.user,
            state=TeamStateTypes.signed_up,
        )
        self.other_team = Team.objects.create(
            name='Waiting',
            beachname='Team',
            tournament=self.tournament,
            trainer=self.user,
            state=TeamStateTypes.waiting,
        )
        self.teamReference = TeamReference.objects.create(
            team=self.team,
            group=self.group
        )
        self.other_teamReference = TeamReference.objects.create(
            from_group=self.other_group,
            group=self.group,
            place=2
        )

    def test_group_structure_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:schedule-group-structure',
                                      kwargs={'pk': self.event.id}))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        tournaments = data['tournaments']
        self.assertIsNotNone(tournaments)
        self.assertEqual(len(tournaments), 1)
        groups = data['groups']
        self.assertIsNotNone(groups)
        self.assertEqual(len(groups), 2)
        teams = data['teams']
        self.assertIsNotNone(teams)
        self.assertEqual(len(teams), 1)
        team_references = data['team_references']
        self.assertIsNotNone(team_references)
        self.assertEqual(len(team_references), 2)

    def test_group_structure_get_unauthorized(self):
        client = APIClient()
        response = client.get(reverse('v1:schedule-group-structure',
                                      kwargs={'pk': self.event.id}))
        self.assertEqual(response.status_code, 401)

    def test_group_structure_get_invalid_token(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT absolute_invalid_token')
        response = client.get(reverse('v1:schedule-group-structure',
                                      kwargs={'pk': self.event.id}))
        self.assertEqual(response.status_code, 401)

    def test_group_structure_get_unkown(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:schedule-group-structure',
                                      kwargs={'pk': 9999999999}))
        self.assertEqual(response.status_code, 404)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'], 'Not found.')

    def test_group_structure_post(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        groups = [{
            'id': '1',
            'name': 'Gruppe A',
            'level': 1,
            'tournament_id': self.tournament.id
        }]
        teams = [{
            'id': '2',
            'team_id': self.team.id,
            'group_id': '1',
            'is_absolute': True
        }, {
            'id': '3',
            'team_id': self.other_team.id,
            'group_id': '1',
            'is_absolute': True
        }]
        games = [{
            'team_a_id': '2',
            'team_b_id': '3',
            'group_id': '1',
            'court': 1,
            'number': 1,
            'datetime': '2013-02-04T22:44:30.652Z',
        }]
        contents = {'groups': groups, 'teams': teams, 'games': games}
        response = client.post(reverse('v1:schedule-group-structure',
                                       kwargs={'pk': self.event.id}),
                               data=json.dumps(contents),
                               content_type='application/json')
        self.assertEqual(response.status_code, 200)
        team_references = TeamReference.objects.all()
        self.assertEqual(team_references.count(), 2)
        self.assertEqual(team_references.first().name, self.team.complete_name)
        self.assertEqual(team_references.last().name, self.other_team.complete_name)
        groups = Group.objects.all()
        self.assertEqual(groups.count(), 1)
        self.assertEqual(groups.first().name, 'Gruppe A')
        games = Game.objects.all()
        self.assertEqual(games.count(), 1)

    def test_group_structure_post_atomic_with_error(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        groups = [{
            'id': '1',
            'name': 'Gruppe A',
            'level': 1,
            'tournament_id': self.tournament.id
        }]
        teams = [{
            'id': '2',
            'team_id': self.team.id,
            'group_id': '912841984214',
            'is_absolute': True
        }, {
            'id': '3',
            'team_id': self.other_team.id,
            'group_id': '1194214198431',
            'is_absolute': True
        }]
        games = [{
            'team_a_id': '2',
            'team_b_id': '3',
            'group_id': '1',
            'court': 1,
            'number': 1,
            'datetime': '2013-02-04T22:44:30.652Z',
        }]
        contents = {'groups': groups, 'teams': teams, 'games': games}
        response = client.post(reverse('v1:schedule-group-structure',
                                       kwargs={'pk': self.event.id}),
                               data=json.dumps(contents),
                               content_type='application/json')
        self.assertNotEqual(response.status_code, 200)
        team_references = TeamReference.objects.all()
        self.assertEqual(team_references.count(), 2)
        self.assertEqual(team_references.first(), self.teamReference)
        self.assertEqual(team_references.last(), self.other_teamReference)
        groups = Group.objects.all()
        self.assertEqual(groups.count(), 2)
        self.assertEqual(groups.first(), self.group)
        self.assertEqual(groups.last(), self.other_group)
        games = Game.objects.all()
        self.assertEqual(games.count(), 0)

    def test_group_structure_post_unauthorized(self):
        client = APIClient()
        response = client.post(reverse('v1:schedule-group-structure',
                                       kwargs={'pk': self.event.id}), {})
        self.assertEqual(response.status_code, 401)

    def test_group_structure_post_unkown(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:schedule-group-structure',
                                       kwargs={'pk': 9999999999}), {})
        self.assertEqual(response.status_code, 404)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'], 'Not found.')

    def test_group_structure_post_keeps_score(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        groups = [{
            'id': '1',
            'name': 'Gruppe A',
            'level': 1,
            'tournament_id': self.tournament.id
        }]
        teams = [{
            'id': '2',
            'team_id': self.team.id,
            'group_id': '1',
            'is_absolute': True
        }, {
            'id': '3',
            'team_id': self.other_team.id,
            'group_id': '1',
            'is_absolute': True
        }]
        games = [{
            'team_a_id': '2',
            'team_b_id': '3',
            'group_id': '1',
            'court': 1,
            'number': 1,
            'datetime': '2013-02-04T22:44:30.652Z',
            'scoreAHalftime1': 5,
            'scoreBHalftime1': 2,
            'scoreAHalftime2': 10,
            'scoreBHalftime2': 3,
            'scoreAHalftime3': None,
            'scoreBHalftime3': None,
        }]
        contents = {'groups': groups, 'teams': teams, 'games': games}
        response = client.post(reverse('v1:schedule-group-structure',
                                       kwargs={'pk': self.event.id}),
                               data=json.dumps(contents),
                               content_type='application/json')
        self.assertEqual(response.status_code, 200)
        games = Game.objects.all()
        self.assertEqual(games.count(), 1)
        game = games.first()
        self.assertEqual(game.scoreAHalftime1, 5)
        self.assertEqual(game.scoreBHalftime1, 2)
        self.assertEqual(game.scoreAHalftime2, 10)
        self.assertEqual(game.scoreBHalftime2, 3)
        self.assertEqual(game.points_a, 2)
        self.assertEqual(game.points_b, 0)
        self.assertTrue(game.is_played)
        team_references = TeamReference.objects.all()
        self.assertEqual(team_references.count(), 2)
        self.assertEqual(team_references.first().points, 2)
        self.assertEqual(team_references.first().goals, 15)
        self.assertEqual(team_references.last().points, 0)
        self.assertEqual(team_references.last().goals, 5)
