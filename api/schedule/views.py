from django.db import transaction, IntegrityError
from rest_framework import permissions, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from api.enums import TeamStateTypes
from api.events.models import Event
from api.games.models import Game
from api.games.serializers import GameSerializer
from api.groups.models import Group
from api.groups.serializers import GroupSerializer
from api.team.models import Team
from api.team.serializers import TeamSerializer
from api.teamreferences.models import TeamReference
from api.teamreferences.serializers import TeamReferenceSerializer
from api.tournaments.models import Tournament
from api.tournaments.serializers import TournamentSerializer
from api.schedule.tasks import save_games


class GroupStructureView(APIView):
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, pk, format=None):
        get_object_or_404(Event.objects.all(), pk=pk)
        tournaments = Tournament.objects.filter(event=pk)\
            .select_related('event')
        groups = Group.objects.filter(tournament__event=pk)\
            .select_related('tournament', 'tournament__event').prefetch_related('teams').order_by('name')
        team_references = TeamReference.objects.filter(group__tournament__event=pk)\
            .select_related('group', 'group__tournament', 'group__tournament__event', 'team', 'from_group')\
            .prefetch_related('gamesA', 'gamesB')
        teams = Team.objects.filter(tournament__event=pk).filter(state=TeamStateTypes.signed_up) \
            .select_related('tournament', 'tournament__event')
        games = Game.objects.filter(event=pk).order_by('number')\
            .select_related('event', 'teamA', 'teamB')

        result = {
            'tournaments': TournamentSerializer(tournaments, many=True,
                                                context={'expand_id_only': ['signed_up_teams']}).data,
            'groups': GroupSerializer(groups, many=True).data,
            'team_references': TeamReferenceSerializer(team_references, many=True).data,
            'teams': TeamSerializer(teams, many=True).data,
            'games': GameSerializer(games, many=True).data,
        }

        return Response(result)

    @transaction.atomic
    def post(self, request, pk, format=None):
        event = get_object_or_404(Event.objects.all(), pk=pk)

        new_groups = []
        new_teams = []
        try:
            with transaction.atomic():
                Group.objects.all().filter(tournament__event=event).delete()
                TeamReference.objects.all().filter(group__tournament__event=event).delete()
                Game.objects.all().filter(event=event).delete()

                group_id_dict = dict()
                team_id_dict = dict()
                new_games = []
                for group in request.data.get('groups'):
                    g = Group(
                        tournament=get_object_or_404(Tournament.objects.all(),
                                                     pk=group.get('tournament_id')),
                        name=group.get('name'),
                        level=group.get('level')
                    )
                    new_groups.append(g)
                    group_id_dict[str(group.get('id'))] = g

                Group.objects.bulk_create(new_groups)

                for team in request.data.get('teams'):
                    group = group_id_dict.get(str(team.get('group_id')))
                    if team.get('is_absolute', True):
                        t = TeamReference(
                            group=group,
                            team=get_object_or_404(Team.objects.all(),
                                                   pk=team.get('team_id')),
                        )
                    else:
                        t = TeamReference(
                            group=group,
                            from_group=group_id_dict.get(str(team.get('from_group_id'))),
                            place=team.get('place')
                        )
                    team_id_dict[str(team.get('id'))] = t
                    new_teams.append(t)

                TeamReference.objects.bulk_create(new_teams)

                for game in request.data.get('games'):
                    group = None
                    team_a = None
                    team_b = None
                    if game.get('group_id') and game.get('team_a_id') and game.get('team_b_id'):
                        group = group_id_dict.get(str(game.get('group_id')))
                        team_a = team_id_dict.get(str(game.get('team_a_id')))
                        team_b = team_id_dict.get(str(game.get('team_b_id')))
                    g = Game(
                        number=game.get('number'),
                        court=game.get('court'),
                        datetime=game.get('datetime'),
                        group=group,
                        teamA=team_a,
                        teamB=team_b,
                        event=event,
                        scoreAHalftime1=game.get('scoreAHalftime1'),
                        scoreBHalftime1=game.get('scoreBHalftime1'),
                        scoreAHalftime2=game.get('scoreAHalftime2'),
                        scoreBHalftime2=game.get('scoreBHalftime2'),
                        scoreAHalftime3=game.get('scoreAHalftime3'),
                        scoreBHalftime3=game.get('scoreBHalftime3'),
                    )
                    new_games.append(g)

                Game.objects.bulk_create(new_games)
        except IntegrityError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        save_games.s([game.id for game in new_games]).apply_async(countdown=5)
        return Response(status=status.HTTP_200_OK)
