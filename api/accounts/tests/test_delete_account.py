import json

from django.contrib.auth import get_user_model
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TransactionTestCase
from django.utils.translation import activate
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from api.accounts.models import MyUser
from api.enums import TeamStateTypes
from api.events.models import Event
from api.team.models import Team
from api.tournaments.models import Tournament

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class DeleteTestCase(TransactionTestCase):
    token = None
    user = None
    team = None

    def setUp(self):
        self.user = MyUser.objects.create(email='test@byom.de',
                                          first_name='Test',
                                          last_name='User',
                                          phone='+49192481024')
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.save()
        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)

        event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        tournament = Tournament.objects \
            .create(name='Test Turnier',
                    gender='mixed',
                    deadline_signup='2017-01-01T00:00:00Z',
                    deadline_edit='2017-01-01T00:00:00Z',
                    advertisement_url='http://www.google.de',
                    contact_email='test@byom.de',
                    starting_fee=60.0,
                    number_of_places=12,
                    event=event
                    )
        self.team = Team.objects.create(
            name='TSV Ismaning',
            beachname='THC Eh Drin!',
            tournament=tournament,
            trainer=self.user,
        )
        Team.objects.create(
            name='Other',
            beachname='Team',
            tournament=tournament,
            trainer=self.user,
        )

    def test_delete_account_unauthorized(self):
        client = APIClient()
        response = client.post(reverse('v1:authemail-delete-account'))
        self.assertEqual(response.status_code, 401)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'],
                         'Authentication credentials were not provided.')
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(Team.objects.all().count(), 2)
        self.assertEqual(len(mail.outbox), 0)

    def test_delete_account_invalid_token(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT absolute_invalid_token')
        response = client.post(reverse('v1:authemail-delete-account'))
        self.assertEqual(response.status_code, 401)
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(Team.objects.all().count(), 2)
        self.assertEqual(len(mail.outbox), 0)

    def test_delete_account_no_confirmation_password(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:authemail-delete-account'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(Team.objects.all().count(), 2)
        self.assertEqual(len(mail.outbox), 0)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'],
                         'No Password for confirmation provided.')

    def test_delete_account_incorrect_confirmation_password(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:authemail-delete-account'), {
            'password': 'not-correct'
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(Team.objects.all().count(), 2)
        self.assertEqual(len(mail.outbox), 0)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'],
                         'The Confirmation Password is incorrect.')

    def test_delete_account_has_signed_up_teams(self):
        self.team.state = TeamStateTypes.signed_up
        self.team.save()

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:authemail-delete-account'), {
            'password': 'test123'
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(Team.objects.all().count(), 2)
        self.assertEqual(len(mail.outbox), 0)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'],
                         'Cannot delete User, because there exist signed up teams.')

    def test_delete_account_success(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:authemail-delete-account'), {
            'password': 'test123'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(get_user_model().objects.all().count(), 0)
        self.assertEqual(Team.objects.all().count(), 0)
        self.assertEqual(len(mail.outbox), 1)
