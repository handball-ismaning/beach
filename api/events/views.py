from rest_framework import viewsets
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin

from api.events.serializers import EventSerializer
from api.permissions import IsAdminOrReadOnly
from .models import Event


class EventViewSet(SerializerExtensionsAPIViewMixin, viewsets.ModelViewSet):
    queryset = Event.objects.all().order_by('start_date')
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = EventSerializer
    pagination_class = None
    extensions_expand_id_only = {'tournaments'}

    def get_extensions_mixin_context(self):
        context = super(EventViewSet, self).get_extensions_mixin_context()

        if self.request.method in ['POST', 'PUT']:
            context['expand'] = set()

        return context
