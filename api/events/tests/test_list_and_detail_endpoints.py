import json

from datetime import timedelta
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.translation import activate
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from api.accounts.models import MyUser
from api.tournaments.models import Tournament
from api.events.models import Event

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

token_regex = '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'


class Events(TestCase):
    token = None
    event = None

    def setUp(self):
        user = MyUser.objects.create(email='test@byom.de', first_name='Test', last_name='User', phone='+49192481024')
        user.set_password('test123')
        user.is_verified = True
        user.is_staff = True
        user.save()
        payload = jwt_payload_handler(user)
        self.token = jwt_encode_handler(payload)

        self.event = Event.objects \
            .create(name='Test Event',
                    start_date='2017-01-01',
                    end_date='2017-01-02',
                    start_time='09:00',
                    end_time='17:00',
                    game_duration=timedelta(minutes=12),
                    pause_duration=timedelta(minutes=3),
                    court_count=3
                    )

    def test_event_list_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:event-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['name'], 'Test Event')
        self.assertGreaterEqual(data[0]['id'], 1)

    def test_event_list_get_dates(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:event-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertTrue(data[0]['start_date'])
        self.assertTrue(data[0]['end_date'])

    def test_event_list_get_times(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:event-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertTrue(data[0]['start_time'])
        self.assertTrue(data[0]['end_time'])

    def test_event_list_get_tournaments(self):
        Tournament.objects.create(
            name='Test Turnier',
            gender='mixed',
            deadline_signup='2017-01-01T00:00:00Z',
            deadline_edit='2017-01-01T00:00:00Z',
            advertisement_url='http://www.google.de',
            contact_email='test@byom.de',
            starting_fee=60.0,
            number_of_places=12,
            event=self.event
        )
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:event-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertTrue(data[0]['tournaments'])
        tournaments = data[0]['tournaments']
        self.assertEqual(len(tournaments), 1)

    def test_event_list_get_unauthorized(self):
        client = APIClient()
        response = client.get(reverse('v1:event-list'))
        self.assertNotEqual(response.status_code, 401)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['name'], 'Test Event')

    def test_event_list_get_invalid_token(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT absolute_invalid_token')
        response = client.get(reverse('v1:event-list'))
        self.assertEqual(response.status_code, 401)

    def test_event_create(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:event-list'), {
            'name': 'Another Event',
            'start_date': '2017-01-02',
            'end_date': '2017-01-02',
            'start_time': '09:00',
            'end_time': '17:00',
            'game_duration': '00:12:00',
            'pause_duration': '00:03:00',
            'court_count': 3,
            'game_mode': 'complex'
        })
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.content.decode('utf-8'))
        self.assertGreater(data['id'], 1)
        self.assertEqual(data['name'], 'Another Event')

        self.assertEqual(Event.objects.all().count(), 2)
        self.assertGreater(Event.objects.last().id, 1)
        self.assertEqual(Event.objects.last().name, 'Another Event')

    def test_event_missing_parameter(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:event-list'), {})
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['name'], ['This field is required.'])
        self.assertEqual(data['start_date'], ['This field is required.'])
        self.assertEqual(data['end_date'], ['This field is required.'])

    def test_event_create_invalid_start_end_date(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:event-list'), {
            'name': 'New Test',
            'start_date': '2017-01-06',
            'end_date': '2017-01-02',
            'game_mode': 'complex'
        })
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['non_field_errors'],
                         ['StartDate must be before EndDate'])

    def test_event_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:event-detail',
                                      kwargs={'pk': self.event.id}))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['name'], 'Test Event')
        self.assertGreaterEqual(data['id'], 1)

    def test_event_get_unkown(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:event-detail',
                                      kwargs={'pk': 9999999999}))
        self.assertEqual(response.status_code, 404)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'], 'Not found.')

    def test_event_put(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.put(reverse('v1:event-detail',
                                      kwargs={'pk': self.event.id}), {
                                  'name': 'New Name',
                                  'start_date': '2017-01-01',
                                  'end_date': '2017-01-02',
                                  'game_mode': 'complex'
                              })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['name'], 'New Name')
        self.assertEqual(Event.objects.first().name, 'New Name')

    def test_event_put_invalid_start_end_date(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.put(reverse('v1:event-detail',
                                      kwargs={'pk': self.event.id}), {
                                  'name': 'New Event',
                                  'start_date': '2017-01-06',
                                  'end_date': '2017-01-02',
                                  'game_mode': 'complex'
                              })
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['non_field_errors'],
                         ['StartDate must be before EndDate'])

    def test_event_delete(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.delete(reverse('v1:event-detail',
                                         kwargs={'pk': self.event.id}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Event.objects.all().count(), 0)
