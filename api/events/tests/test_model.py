from datetime import timedelta
from django.test import TestCase
from django.utils.translation import activate

from api.accounts.models import MyUser
from api.events.models import Event

activate('en-us')


class Events(TestCase):
    event = None
    user = None

    def setUp(self):
        self.event = Event.objects \
            .create(name='Test Turnier',
                    start_date='2017-01-01',
                    end_date='2017-01-02',
                    start_time='09:00',
                    end_time='17:00',
                    game_duration=timedelta(minutes=12),
                    pause_duration=timedelta(minutes=3),
                    court_count=3,
                    )
        self.user = MyUser.objects.create(email='test@byom.de',
                                          first_name='Test',
                                          last_name='User',
                                          phone='+49192481024')
        self.user.set_password('test123')
        self.user.is_verified = True
        self.user.is_staff = True
        self.user.save()

    def test_fields(self):
        self.assertEqual(self.event.name, 'Test Turnier')
        self.assertEqual(self.event.start_date, '2017-01-01')
        self.assertEqual(self.event.end_date, '2017-01-02')

    def test_total_game_duration_in_minutes(self):
        self.assertEqual(self.event.total_game_duration_in_minutes, 15)

        self.event.game_duration = timedelta(hours=2)
        del self.event.total_game_duration_in_minutes  # to invalidate the cache
        self.assertEqual(self.event.total_game_duration_in_minutes, 2 * 60 + 3)
