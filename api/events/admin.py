from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from api.tournaments.models import Tournament
from .models import Event


class TournamentsInline(admin.TabularInline):
    model = Tournament
    extra = 0
    show_change_link = True
    fields = ('name', 'gender', 'start_signup', 'deadline_signup', 'deadline_edit', 'starting_fee', 'number_of_places')
    ordering = ('name', 'gender')
    classes = ('collapse',)


class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'start_date', 'end_date', 'game_mode')
    fieldsets = (
        (None, {'fields': ('name', 'image')}),
        (_('Date'), {'fields': ('start_date', 'end_date')}),
        (_('Game Schedule'), {'fields': ('start_time',
                                         'end_time',
                                         'game_duration',
                                         'pause_duration',
                                         'court_count',
                                         'game_mode',
                                         'print_player_list',
                                         'schedule_public')})
    )
    inlines = [TournamentsInline]


admin.site.register(Event, EventAdmin)
