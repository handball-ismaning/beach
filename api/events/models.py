from datetime import timedelta

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from api.enums import GameModeTypes


class Event(models.Model):
    name = models.CharField(max_length=120,
                            help_text=_("Name of the Tournament"),
                            verbose_name=_("Tournament Name"))
    start_date = models.DateField(help_text=_("The first day when the event takes place"),
                                  verbose_name=_("Start Date of the Event"))
    end_date = models.DateField(help_text=_("The last day when the event takes place"),
                                verbose_name=_("End Date of the Event"))
    start_time = models.TimeField(help_text=_("The start time of the game schedule for each day"),
                                  verbose_name=_("Start Time"), null=True)
    end_time = models.TimeField(help_text=_("The end time of the game schedule for each day"),
                                verbose_name=_("End Time"), null=True)
    game_duration = models.DurationField(help_text=_("The duration of one game (HH:MM:SS)"),
                                         verbose_name=_("Game Duration"), null=True)
    pause_duration = models.DurationField(help_text=_("The duration of the pause between games (HH:MM:SS)"),
                                          verbose_name=_("Pause Duration"), null=True)
    court_count = models.PositiveSmallIntegerField(help_text=_("The Amount of Courts that are played on"),
                                                   verbose_name=_("Count Courts"), default=1,
                                                   validators=[MinValueValidator(1)])
    game_mode = models.CharField(
        max_length=50,
        choices=GameModeTypes.choices, default=GameModeTypes.complex,
        help_text=_("Game Mode for each Game (1 Halftime / 2 Halftimes + Penalty)"),
        verbose_name=_("Game Mode")
    )
    print_player_list = models.BooleanField(
        help_text=_("Print the Player List for each Team on Game Print Page"),
        verbose_name=_("Print Player List"),
        default=True
    )
    image = models.URLField(
        help_text=_("Image Url that is shown on the landing page"),
        verbose_name=_("Image URL"),
        default=None,
        blank=True,
        null=True
    )
    schedule_public = models.BooleanField(
        help_text=_("Boolean wether the schedules are publicly available"),
        verbose_name=_("Schedule Public"),
        default=True
    )

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')

    @cached_property
    def total_game_duration_in_minutes(self):
        game_duration = self.game_duration if self.game_duration else timedelta()
        pause_duration = self.pause_duration if self.pause_duration else timedelta()
        return round((game_duration.total_seconds() + pause_duration.total_seconds()) / 60)

    def clean(self):
        if self.start_date and self.end_date and \
                self.start_date > self.end_date:
            raise ValidationError(
                _('StartDate must be before EndDate')
            )

    def __str__(self):
        return self.name
