from django.conf.urls import url

from .views import EventViewSet

event_list = EventViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
event_detail = EventViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    url(r'^$', event_list, name='event-list'),
    url(r'^(?P<pk>[0-9]+)/$', event_detail, name='event-detail'),
]
