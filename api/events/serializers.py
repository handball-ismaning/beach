from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin

from api.enums import GameModeTypes
from api.events.models import Event


class EventSerializer(SerializerExtensionsMixin, serializers.ModelSerializer):
    game_mode = serializers.ChoiceField(choices=GameModeTypes.choices)

    class Meta:
        model = Event
        fields = ('id', 'name', 'start_date', 'end_date', 'game_mode', 'image',
                  'start_time', 'end_time', 'game_duration', 'pause_duration', 'schedule_public',
                  'total_game_duration_in_minutes', 'court_count', 'print_player_list')
        read_only_fields = ('id', 'total_game_duration_in_minutes', 'image', 'tournaments', 'schedule_public')
        expandable_fields = dict(
            tournaments=dict(
                serializer='api.tournaments.serializers.TournamentSerializer',
                read_only=True,
                many=True,
            )
        )

    def validate(self, data):
        start_date = data.get('start_date', self.instance.start_date if self.instance else None)
        end_date = data.get('end_date', self.instance.end_date if self.instance else None)

        if start_date > end_date:
            raise serializers.ValidationError(
                _('StartDate must be before EndDate')
            )

        start_time = data.get('start_time', self.instance.start_time if self.instance else None)
        end_time = data.get('end_time', self.instance.end_time if self.instance else None)

        if start_time > end_time:
            raise serializers.ValidationError(
                _('StartTime must be before EndTime')
            )

        return data
