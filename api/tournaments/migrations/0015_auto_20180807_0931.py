# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-08-07 07:31
from __future__ import unicode_literals

import colorful.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tournaments', '0014_tournament_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tournament',
            name='color',
            field=colorful.fields.RGBColorField(colors=['#FF0000', '#00FF00', '#0000FF', '#409EFF', '#67C23A', '#E6A23C', '#F56C6C', '#909399'], default='#909399', help_text='Color that the tournament is displayed with', verbose_name='Color'),
        ),
    ]
