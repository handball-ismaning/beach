from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from api.team.models import Team
from .models import Tournament, AdditionalDocument


class DocumentsInline(admin.StackedInline):
    model = AdditionalDocument
    fields = ('name', 'url')
    extra = 0


class TeamsInline(admin.TabularInline):
    model = Team
    extra = 0
    show_change_link = True
    fields = ('name', 'beachname', 'trainer', 'paid', 'state')
    ordering = ('state', 'date_signup')
    classes = ('collapse',)


class TournamentAdmin(admin.ModelAdmin):
    list_display = ('name', 'gender', 'event', 'signup_open',
                    'deadline_signup', 'count_signed_up_teams')
    fieldsets = (
        (None, {'fields': ('name', 'gender', 'event')}),
        (_('Deadlines'), {'fields': ('start_signup',
                                     'deadline_signup',
                                     'deadline_edit')}),
        (_('General'), {'fields': ('advertisement_url',
                                   'contact_email',
                                   'starting_fee',
                                   'number_of_places',
                                   'color')}),
        (_('Schedule Dates'), {'fields': ('start_date_games',
                                          'end_date_games')})
    )
    inlines = [DocumentsInline, TeamsInline]


admin.site.register(Tournament, TournamentAdmin)
