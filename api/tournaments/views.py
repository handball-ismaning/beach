from rest_framework import viewsets
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin

from api.permissions import IsAdminOrReadOnly
from .models import Tournament
from .serializers import TournamentSerializer


class TournamentViewSet(SerializerExtensionsAPIViewMixin, viewsets.ModelViewSet):
    queryset = Tournament.objects.all().order_by('event__start_date')
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = TournamentSerializer
    pagination_class = None
    extensions_expand_id_only = {'signed_up_teams'}
