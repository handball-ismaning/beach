import datetime
import json

from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone
from django.utils.translation import activate
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from api.accounts.models import MyUser
from api.events.models import Event
from api.tournaments.models import Tournament

activate('en-us')

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

token_regex = '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'


class Tournaments(TestCase):
    token = None
    tournament = None
    event = None

    def setUp(self):
        user = MyUser.objects.create(email='test@byom.de', first_name='Test', last_name='User', phone='+49192481024')
        user.set_password('test123')
        user.is_verified = True
        user.is_staff = True
        user.save()
        payload = jwt_payload_handler(user)
        self.token = jwt_encode_handler(payload)

        self.event = Event.objects.create(
            name='Test Event',
            start_date='2017-01-01',
            end_date='2017-01-02'
        )
        self.tournament = Tournament.objects \
            .create(name='Test Turnier',
                    gender='mixed',
                    deadline_signup='2017-01-01T00:00:00Z',
                    deadline_edit='2017-01-01T00:00:00Z',
                    advertisement_url='http://www.google.de',
                    contact_email='test@byom.de',
                    starting_fee=60.0,
                    number_of_places=12,
                    event=self.event
                    )

    def test_tournament_list_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:tournament-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['name'], 'Test Turnier')
        self.assertGreaterEqual(data[0]['id'], 1)

    def test_tournament_list_get_signup_flags(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:tournament-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertFalse(data[0]['signup_open'])
        self.assertFalse(data[0]['is_before_signup'])
        self.assertTrue(data[0]['is_after_signup'])

    def test_tournament_list_get_unauthorized(self):
        client = APIClient()
        response = client.get(reverse('v1:tournament-list'))
        self.assertNotEqual(response.status_code, 401)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['name'], 'Test Turnier')

    def test_tournament_list_get_invalid_token(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT absolute_invalid_token')
        response = client.get(reverse('v1:tournament-list'))
        self.assertEqual(response.status_code, 401)

    def test_tournament_get(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:tournament-detail',
                                      kwargs={'pk': self.tournament.id}))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['name'], 'Test Turnier')
        self.assertGreaterEqual(data['id'], 1)

    def test_tournament_get_unkown(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:tournament-detail',
                                      kwargs={'pk': 9999999999}))
        self.assertEqual(response.status_code, 404)
        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(data['detail'], 'Not found.')

    def test_tournament_get_signup_flags(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.get(reverse('v1:tournament-detail',
                                      kwargs={'pk': self.tournament.id}))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode('utf-8'))
        self.assertFalse(data['signup_open'])
        self.assertFalse(data['is_before_signup'])
        self.assertTrue(data['is_after_signup'])

    def test_tournament_post(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.post(reverse('v1:tournament-list'), {})
        self.assertEqual(response.status_code, 405)

    def test_tournament_put(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.put(reverse('v1:tournament-detail',
                                      kwargs={'pk': self.tournament.id}), {})
        self.assertEqual(response.status_code, 405)

    def test_tournament_delete(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        response = client.delete(reverse('v1:tournament-detail',
                                         kwargs={'pk': self.tournament.id}))
        self.assertEqual(response.status_code, 405)

    def test_tournament_signup_open(self):
        self.tournament.deadline_signup = timezone.now() + \
                                          datetime.timedelta(days=1)
        self.tournament.start_signup = timezone.now() - \
            datetime.timedelta(days=1)
        self.tournament.save()

        self.assertFalse(self.tournament.is_before_signup)
        self.assertFalse(self.tournament.is_after_signup)
        self.assertTrue(self.tournament.signup_open)

    def test_tournament_signup_open_before(self):
        self.tournament.deadline_signup = timezone.now() + \
            datetime.timedelta(days=2)
        self.tournament.start_signup = timezone.now() + \
            datetime.timedelta(days=1)
        self.tournament.save()

        self.assertTrue(self.tournament.is_before_signup)
        self.assertFalse(self.tournament.is_after_signup)
        self.assertFalse(self.tournament.signup_open)

    def test_tournament_signup_open_after(self):
        self.tournament.deadline_signup = timezone.now() - \
            datetime.timedelta(days=1)
        self.tournament.start_signup = timezone.now() - \
            datetime.timedelta(days=2)
        self.tournament.save()

        self.assertFalse(self.tournament.is_before_signup)
        self.assertTrue(self.tournament.is_after_signup)
        self.assertFalse(self.tournament.signup_open)
