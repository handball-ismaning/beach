from django.conf.urls import url, include

from . import views

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^account/', include('api.accounts.urls')),
    url(r'^events/', include('api.events.urls')),
    url(r'^tournaments/', include('api.tournaments.urls')),
    url(r'^teams/', include('api.team.urls')),
    url(r'^players/', include('api.players.urls')),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^config/', views.get_config, name='config'),
    url(r'^groups/', include('api.groups.urls')),
    url(r'^teamreferences/', include('api.teamreferences.urls')),
    url(r'^games/', include('api.games.urls')),
    url(r'^schedule/', include('api.schedule.urls')),
    url('^', views.raise_404)
]
