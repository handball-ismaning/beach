# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).



## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 1.5.1 - (2023-02-24)
---

### Fixes
* Allow Backend tests to fail in CI pipeline to be able to deploy


## 1.5.0 - (2020-02-18)
---

### New
* Added Group Pairing Table as Distribution Strategy


### Fixes
* Fix visibility of player delete button


## 1.4.3 - (2020-02-14)
---

### Fixes
* Added retries to celery tasks


## 1.4.2 - (2020-02-14)
---

### Changes
* Added Celery worker to Heroku Procfile


## 1.4.1 - (2020-02-14)
---

### Changes
* Use Celery and Redis for asynchronous tasks


## 1.4.0 - (2020-02-13)
---

### New
* Added Hash Check for Schedule distribution check

### Changes
* Refactored Schedule distributor
* Moved expensive post_save signals to background thread

### Fixes
* Fixed issues with handling a score of 0
* Fixed Click on team in Game table not redirecting to team


## 1.3.9 - (2020-01-20)
---

### Fixes
* Fixed flakey test


## 1.3.8 - (2020-01-20)
---

### Fixes
* Fixed Height Bug in bottom message


## 1.3.7 - (2019-07-23)
---

### Fixes
* Fixed Schedule Level Add Issue


## 1.3.6 - (2019-07-18)
---

### Fixes
* Reworked Schedule Edit Page for faster Rendering


## 1.3.5 - (2019-07-16)
---

### Fixes
* Fixed Excel Export Field Names


## 1.3.4 - (2019-07-15)
---

### Fixes
* Updated Vue Json Excel Dependency to fix bug with game export


## 1.3.3 - (2019-06-08)
---

### Fixes
* Fixed Dependency Version issue


## 1.3.2 - (2019-06-08)
---

### Fixes
* Fixed Dependency Version issue


## 1.3.1 - (2019-06-07)
---

### Fixes
* Fixed Dependency Version issue


## 1.3.0 - (2019-06-07)
---

### New
* Added Option to make Schedules public or private
* Added Schedule Export

### Changes
* Adjusted Game Print Template


## 1.2.5 - (2019-02-04)
---

### Fixes
* Show correct date in tournament card
* Fixed Sorting order of tournaments in registration
* Fixed not expanded trainer on team update


## 1.2.4 - (2019-02-01)
---

### Fixes
* Updated E-Mails on Contact Page


## 1.2.3 - (2018-10-22)
---

### Changes
* Updated Service Desk Email


## 1.2.2 - (2018-09-04)
---

### Fixes
* fixed bug with team loading in game schedule editor


## 1.2.1 - (2018-09-04)
---

### Fixes
* make schedule retain game results
* fixed bug with not all required fields in inline admiN
* moved teamreference games count to save signals for better performance
* Improved Request Order on frontend routing
* Improved Performance Group Structure load
* Keeep game score with group_structure save
* Improved Group Structure Save


## 1.2.0 - (2018-09-03)
---

### New
* Added Ability to delete account data
* Added Ability to download all data to account page

### Changes
* Added Trainer Data to tournament export

### Fixes
* Fixed Player Retrieval Warning
* Also Print Tournament Gender in All Players export


## 1.1.1 - (2018-09-01)
---

### Changes
* Replaced Event Image File with Url Field


## 1.1.0 - (2018-09-01)
---

### New
* Added Schedule Print Page

### Changes
* Updated HTML head and og tags


## 1.0.0 - (2018-08-30)
---

### New
* Added Trainer Data to team

### Changes
* Adapt Frontend to improved backend serialization
* Make Backend not return nested serializer representations, but ids


## 0.3.0 - (2018-08-29)
---

### New
* Added Referee Integration
* Added PlayerListPrint option to event
* Added Time Delay Insertion
* Added Ability to distribute games within schedule
* Added Validation to schedule
* Added Game Drag and Drop to Schedule Edit Page
* Enhanced Game Schedule endpoint to support game saving and retrieval
* Added Game Creation to Schedule edit page
* Added Group Structure Edit Ability
* Added game Start and End Date to Tournament

### Changes
* Visually separate days in game table
* Added Phone Number to team page
* Added Place In group to admin page, to allow override


## 0.2.1 - (2018-08-15)
---

### Fixes
* fixed bug with correct team resolving
* Fixed build error due to hex-rgb library


## 0.2.0 - (2018-08-14)
---

### New
* Added Game Update Functionality
* Added Game Page
* Added Team Reference Resolve
* Added Game Print Page
* Added Event Images
* Added Colored Tournament Badge
* Added games count to team references
* Added Group Table to group page
* Added Team Table to group model
* Added Game Schedule views to frontend
* Added Game Model
* Added Team Reference Model
* Added Group Model
* Added Single Event page
* Group Tournaments on Anmeldung Home by Events
* Added Events to Landing Page
* Added additional fields to event model
* Tournament now has a reference to event
* Added Event Model and Endpoints to backend

### Changes
* Improved Performance
* Moved Team Table Generation to database
* Reworked displaying of game results in game table
* Moved Anmeldung Routes to suburl
* Moved Anmeldung Home to separate page
* Changed Contact Email adress to gitlab service desk email
* Removed POST, PUT, DELETE Endpoints for tournament


## 0.1.0 - (2018-07-28)
---

### New
* Imported Beachanmeldung Code


