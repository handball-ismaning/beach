/* ============
 * game Transformer
 * ============
 *
 * The transformer for the game.
 */

import { parseDate } from '@/utils/helpers';
import Transformer from './transformer';
import EventTransformer from './event';
import GroupTransformer from './group';
import TeamReferenceTransformer from './teamReference';

export default class GameTransformer extends Transformer {
  /**
   * Method used to transform a fetched game
   *
   * @param game The fetched game
   *
   * @returns {Object} The transformed game
   */
  static fetch(game) {
    if (typeof game !== 'object') return game;
    return {
      id: game.id,
      number: game.number,
      datetime: parseDate(game.datetime),
      court: game.court,
      isPlayed: game.is_played,
      event: game.event ? EventTransformer.fetch(game.event) : undefined,
      group: game.group ? GroupTransformer.fetch(game.group) : undefined,
      groupID: game.group_id,
      teamA: game.teamA ? TeamReferenceTransformer.fetch(game.teamA) : undefined,
      teamAID: game.teamA_id,
      teamB: game.teamB ? TeamReferenceTransformer.fetch(game.teamB) : undefined,
      teamBID: game.teamB_id,
      scoreAHalftime1: game.scoreAHalftime1,
      scoreBHalftime1: game.scoreBHalftime1,
      scoreAHalftime2: game.scoreAHalftime2,
      scoreBHalftime2: game.scoreBHalftime2,
      scoreAHalftime3: game.scoreAHalftime3,
      scoreBHalftime3: game.scoreBHalftime3,
      pointsA: game.points_a,
      pointsB: game.points_b,
      isEmpty: game.is_empty,
      setsA: game.sets_a,
      setsB: game.sets_b,
      referees: game.referees,
    };
  }

  /**
   * Method used to transform a send game
   *
   * @param game The game to be send
   *
   * @returns {Object} The transformed game
   */
  static send(game) {
    return {
      id: game.id,
      scoreAHalftime1: game.scoreAHalftime1,
      scoreBHalftime1: game.scoreBHalftime1,
      scoreAHalftime2: game.scoreAHalftime2,
      scoreBHalftime2: game.scoreBHalftime2,
      scoreAHalftime3: game.scoreAHalftime3,
      scoreBHalftime3: game.scoreBHalftime3,
      referees: game.referees,
    };
  }
}
