/* ============
 * event Transformer
 * ============
 *
 * The transformer for the event.
 */

import { displayDateRange, parseDate, parseDateToISOString, transformArrayConditionally } from '@/utils/helpers';
import Transformer from './transformer';
import TournamentTransformer from './tournament';

export default class EventTransformer extends Transformer {
  /**
   * Method used to transform a fetched event
   *
   * @param event The fetched event
   *
   * @returns {Object} The transformed event
   */
  static fetch(event) {
    if (typeof event !== 'object') return event;
    return {
      id: event.id,
      name: event.name,
      startDate: parseDate(event.start_date),
      endDate: parseDate(event.end_date),
      eventDate: displayDateRange(event.start_date, event.end_date),
      startTime: event.start_time,
      endTime: event.end_time,
      gameDuration: event.game_duration,
      pauseDuration: event.pause_duration,
      totalGameDuration: event.total_game_duration_in_minutes,
      courtCount: event.court_count,
      tournaments: transformArrayConditionally(event.tournaments, TournamentTransformer.fetch),
      gameMode: event.game_mode,
      image: event.image,
      printPlayerList: event.print_player_list,
      schedulePublic: event.schedule_public,
    };
  }

  /**
   * Method used to transform a send event
   *
   * @param event The event to be send
   *
   * @returns {Object} The transformed event
   */
  static send(event) {
    return {
      id: event.id,
      name: event.name,
      start_date: parseDateToISOString(event.startDate),
      end_date: parseDateToISOString(event.endDate),
      start_time: event.startTime,
      end_time: event.endTime,
      game_duration: event.gameDuration,
      pause_duration: event.pauseDuration,
      court_count: event.courtCount,
    };
  }
}
