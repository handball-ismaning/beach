/* ============
 * group Transformer
 * ============
 *
 * The transformer for the group.
 */

import Transformer from './transformer';
import TournamentTransformer from './tournament';
import TeamReferenceTransformer from './teamReference';

import { transformArrayConditionally, transformObjectConditionally } from '@/utils/helpers';

export default class GroupTransformer extends Transformer {
  /**
   * Method used to transform a fetched group
   *
   * @param group The fetched group
   *
   * @returns {Object} The transformed group
   */
  static fetch(group) {
    if (typeof group !== 'object') return group;
    return {
      id: group.id,
      name: group.name,
      level: group.level,
      tournamentID: group.tournament_id,
      tournament: transformObjectConditionally(group.tournament, TournamentTransformer.fetch),
      teams: transformArrayConditionally(group.teams, TeamReferenceTransformer.fetch),
      teamTable: transformArrayConditionally(group.team_table, TeamReferenceTransformer.fetch),
      countTeams: group.teams ? group.teams.length : 0,
    };
  }

  /**
   * Method used to transform a send group
   *
   * @param group The group to be send
   *
   * @returns {Object} The transformed group
   */
  static send(group) {
    return {
      id: group.id,
      name: group.name,
    };
  }
}
