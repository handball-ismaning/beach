/* ============
 * Tournament Transformer
 * ============
 *
 * The transformer for the tournament.
 */

import { displayDateRange, parseDate, parseDateToISOString, transformArrayConditionally } from '@/utils/helpers';
import Transformer from './transformer';
import GroupTransformer from './group';
import TeamTransformer from './team';

export default class TournamentTransformer extends Transformer {
  /**
   * Method used to transform a fetched tournament
   *
   * @param tournament The fetched tournament
   *
   * @returns {Object} The transformed tournament
   */
  static fetch(tournament) {
    if (typeof tournament !== 'object') return tournament;
    return {
      id: tournament.id,
      name: tournament.name,
      gender: tournament.gender,
      startDate: parseDate(tournament.start_date),
      endDate: parseDate(tournament.end_date),
      startSignup: parseDate(tournament.start_signup),
      deadlineSignup: parseDate(tournament.deadline_signup),
      deadlineEdit: parseDate(tournament.deadline_edit),
      advertisementUrl: tournament.advertisement_url,
      contactEmail: tournament.contact_email,
      signupOpen: tournament.signup_open,
      startingFee: tournament.starting_fee,
      isBeforeSignup: tournament.is_before_signup,
      isAfterSignup: tournament.is_after_signup,
      numberOfPlaces: tournament.number_of_places,
      tournamentDate: displayDateRange(tournament.start_date_games, tournament.end_date_games),
      eventID: tournament.event ? tournament.event.id : tournament.event_id,
      eventName: tournament.event ? tournament.event.name : undefined,
      totalCountTeams: tournament.total_count_teams,
      countSignedUpTeams: tournament.count_signed_up_teams,
      freePlaces: tournament.free_places,
      waitlistCount: tournament.waitlist_count,
      approvalCount: tournament.approval_count,
      noPlacesLeft: tournament.no_places_left_flag,
      fewPlacesLeft: tournament.few_places_left_flag,
      groups: tournament.groups ? GroupTransformer.fetchCollection(tournament.groups) : [],
      color: tournament.color,
      signedUpTeams: transformArrayConditionally(tournament.signed_up_teams,
        TeamTransformer.fetch),
      startDateGames: parseDate(tournament.start_date_games),
      endDateGames: parseDate(tournament.end_date_games),
    };
  }

  /**
   * Method used to transform a send tournament
   *
   * @param tournament The tournament to be send
   *
   * @returns {Object} The transformed tournament
   */
  static send(tournament) {
    return {
      id: tournament.id,
      name: tournament.name,
      gender: tournament.gender,
      start_date: parseDateToISOString(tournament.startDate),
      end_date: parseDateToISOString(tournament.endDate),
      start_signup: parseDateToISOString(tournament.startSignup),
      deadline_signup: parseDateToISOString(tournament.deadlineSignup),
      deadline_edit: parseDateToISOString(tournament.deadlineEdit),
      advertisement_url: tournament.advertisementUrl,
      contact_email: tournament.contactEmail,
      signup_open: tournament.signupOpen,
      starting_fee: tournament.startingFee,
      number_of_places: tournament.numberOfPlaces,
    };
  }
}
