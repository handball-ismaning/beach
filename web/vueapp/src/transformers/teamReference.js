/* ============
 * teamReference Transformer
 * ============
 *
 * The transformer for the teamReference.
 */

import Transformer from './transformer';
import TeamTransformer from './team';
import GroupTransformer from './group';


import { transformObjectConditionally } from '@/utils/helpers';

export default class TeamReferenceTransformer extends Transformer {
  /**
   * Method used to transform a fetched teamReference
   *
   * @param teamReference The fetched teamReference
   *
   * @returns {Object} The transformed teamReference
   */
  static fetch(teamReference) {
    if (typeof teamReference !== 'object') return teamReference;
    return {
      id: teamReference.id,
      name: teamReference.name,
      isAbsolute: teamReference.is_absolute,
      isRelative: teamReference.is_relative,
      isResolved: teamReference.is_resolved,
      resolvedTeam: teamReference.is_relative ?
        transformObjectConditionally(teamReference.resolved_team, TeamTransformer.fetch) :
        transformObjectConditionally(teamReference.team, TeamTransformer.fetch),
      resolvedTeamID: teamReference.is_relative ?
        teamReference.resolved_team_id : teamReference.team_id,
      team: transformObjectConditionally(teamReference.team, TeamTransformer.fetch),
      teamID: teamReference.team_id,
      place: teamReference.place,
      group: teamReference.group,
      groupID: teamReference.group_id,
      fromGroup: transformObjectConditionally(teamReference.from_group,
        GroupTransformer.fetch),
      fromGroupID: teamReference.from_group_id,
      points: teamReference.points,
      pointsNegative: teamReference.points_negative,
      pointsDiff: teamReference.points_diff,
      sets: teamReference.sets,
      setsNegative: teamReference.sets_negative,
      setsDiff: teamReference.sets_diff,
      goals: teamReference.goals,
      goalsNegative: teamReference.goals_negative,
      goalsDiff: teamReference.goals_diff,
      gamesCount: teamReference.games_count,
      gamesPlayedCount: teamReference.games_played_count,
    };
  }

  /**
   * Method used to transform a send teamReference
   *
   * @param teamReference The teamReference to be send
   *
   * @returns {Object} The transformed teamReference
   */
  static send(teamReference) {
    return {
      id: teamReference.id,
      team: teamReference.team,
      group: teamReference.group,
      from_group: teamReference.fromGroup,
      place: teamReference.place,
    };
  }
}
