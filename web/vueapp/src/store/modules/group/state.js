/* ============
 * State of the group module
 * ============
 *
 * The initial state of the group module.
 */

export default {
  activeGroup: undefined,
  groups: [],
};
