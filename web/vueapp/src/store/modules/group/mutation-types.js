/* ============
 * Mutation types for the group module
 * ============
 *
 * The mutation types that are available
 * on the group module.
 */

export const SET_ACTIVE = 'SET_ACTIVE';
export const STORE = 'STORE';
export const UPDATE = 'UPDATE';

export default {
  SET_ACTIVE,
  STORE,
  UPDATE,
};
