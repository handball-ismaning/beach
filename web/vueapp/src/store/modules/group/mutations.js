/* ============
 * Mutations for the group module
 * ============
 *
 * The mutations that are available on the
 * group module.
 */

import { SET_ACTIVE, STORE, UPDATE } from './mutation-types';
import { updateOrAddObjToArray } from '@/utils/helpers';

export default {
  [SET_ACTIVE](state, group) {
    state.activeGroup = group;
  },
  [STORE](state, groups) {
    state.groups = groups || [];
  },
  [UPDATE](state, group) {
    state.groups = updateOrAddObjToArray(state.groups, group);
  },
};
