/* ============
 * Actions for the group module
 * ============
 *
 * The actions that are available on the
 * group module.
 */

import * as types from './mutation-types';

export const setActive = ({ commit }, payload) => {
  commit(types.SET_ACTIVE, payload);
};

export const store = ({ commit }, payload) => {
  commit(types.STORE, payload);
};

export default {
  setActive,
  store,
};
