/* ============
 * Getters for the group module
 * ============
 *
 * The getters that are available on the
 * group module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  groupIDLookup(state) {
    return generateIDLookup(state.groups);
  },

  groups(state, getters, rootState, rootGetters) {
    return state.groups.map(group => ({
      ...group,
      teams: group.teams.map(team => rootGetters['teamreference/teamreferenceIDLookup'][team]),
      teamTable: group.teamTable.map(team => rootGetters['teamreference/teamreferenceIDLookup'][team]),
      tournament: rootGetters['tournament/tournamentIDLookup'][group.tournamentID],
    }));
  },

  activeGroup(state, getters) {
    if (!state.activeGroup || !state.activeGroup.id) return undefined;
    return getters.groups.find(single => single.id === state.activeGroup.id);
  },
};
