/* ============
 * State of the game module
 * ============
 *
 * The initial state of the game module.
 */

export default {
  games: [],
  activeGame: undefined,
};
