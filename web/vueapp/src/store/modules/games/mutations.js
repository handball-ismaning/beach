/* ============
 * Mutations for the game module
 * ============
 *
 * The mutations that are available on the
 * game module.
 */

import { STORE, SET_ACTIVE, UPDATE, UPDATE_OR_ADD } from './mutation-types';
import { updateOrAddObjToArray } from '@/utils/helpers';

export default {
  [STORE](state, games) {
    state.games = games || [];
  },
  [SET_ACTIVE](state, game) {
    state.activeGame = game;
  },
  [UPDATE](state, game) {
    state.games = state.games.map(current => (current.id === game.id ? game : current));
  },
  [UPDATE_OR_ADD](state, game) {
    state.games = updateOrAddObjToArray(state.games, game);
  },
};
