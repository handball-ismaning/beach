/* ============
 * Actions for the game module
 * ============
 *
 * The actions that are available on the
 * game module.
 */

import * as types from './mutation-types';

export const store = ({ commit }, payload) => {
  commit(types.STORE, payload);
};

export const setActive = ({ commit }, payload) => {
  commit(types.SET_ACTIVE, payload);
};

export const updateSingle = ({ commit, state }, payload) => {
  commit(types.UPDATE, payload);
  if (state.activeGame && state.activeGame.id === payload.id) {
    commit(types.SET_ACTIVE, payload);
  }
};

export const update = ({ commit, state }, payload) => {
  payload.forEach((game) => {
    commit(types.UPDATE, game);
  });
};

export const updateOrAdd = ({ commit }, payload) => {
  commit(types.UPDATE_OR_ADD, payload);
};

export default {
  store,
  setActive,
  updateSingle,
  update,
  updateOrAdd,
};
