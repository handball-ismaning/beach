/* ============
 * Mutation types for the game module
 * ============
 *
 * The mutation types that are available
 * on the game module.
 */

export const STORE = 'STORE';
export const SET_ACTIVE = 'SET_ACTIVE';
export const UPDATE = 'UPDATE';
export const UPDATE_OR_ADD = 'UPDATE_OR_ADD';

export default {
  STORE,
  SET_ACTIVE,
  UPDATE,
  UPDATE_OR_ADD,
};
