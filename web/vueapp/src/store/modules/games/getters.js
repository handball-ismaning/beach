/* ============
 * Getters for the game module
 * ============
 *
 * The getters that are available on the
 * game module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  gameIDLookup(state) {
    return generateIDLookup(state.games);
  },

  games(state, getters, rootState, rootGetters) {
    return state.games.map((game) => {
      const group = rootGetters['group/groupIDLookup'][game.groupID];
      return {
        ...game,
        event: rootGetters['event/eventIDLookup'][game.eventID],
        tournament: group ? rootGetters['tournament/tournamentIDLookup'][group.tournamentID] : undefined,
        group,
        teamA: rootGetters['teamreference/teamreferenceIDLookup'][game.teamAID],
        teamB: rootGetters['teamreference/teamreferenceIDLookup'][game.teamBID],
      };
    });
  },

  activeGame(state, getters) {
    if (!state.activeGame || !state.activeGame.id) return undefined;
    return getters.games.find(single => single.id === state.activeGame.id);
  },
};
