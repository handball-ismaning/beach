/* ============
 * Getters for the event module
 * ============
 *
 * The getters that are available on the
 * event module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  eventIDLookup(state) {
    return generateIDLookup(state.events);
  },

  events(state, getters, rootState, rootGetters) {
    return state.events.map(event => ({
      ...event,
      tournaments: event.tournaments.map(tournament => rootGetters['tournament/tournamentIDLookup'][tournament]),
    }));
  },

  activeEvent(state, getters) {
    if (!state.activeEvent || !state.activeEvent.id) return undefined;
    return getters.events.find(single => single.id === state.activeEvent.id);
  },
};
