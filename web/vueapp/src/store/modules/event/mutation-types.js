/* ============
 * Mutation types for the event module
 * ============
 *
 * The mutation types that are available
 * on the event module.
 */

export const STORE = 'STORE';
export const SET_ACTIVE = 'SET_ACTIVE';

export default {
  STORE,
  SET_ACTIVE,
};
