/* ============
 * Mutations for the event module
 * ============
 *
 * The mutations that are available on the
 * event module.
 */

import { STORE, SET_ACTIVE } from './mutation-types';

export default {
  [STORE](state, events) {
    state.events = events || [];
  },
  [SET_ACTIVE](state, event) {
    state.activeEvent = event;
  },
};
