/* ============
 * Mutation types for the tournament module
 * ============
 *
 * The mutation types that are available
 * on the tournament module.
 */

export const STORE = 'STORE';
export const SET_ACTIVE = 'SET_ACTIVE';
export const UPDATE = 'UPDATE';

export default {
  STORE,
  SET_ACTIVE,
  UPDATE,
};
