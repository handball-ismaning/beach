/* ============
 * Mutations for the tournament module
 * ============
 *
 * The mutations that are available on the
 * tournament module.
 */

import { STORE, SET_ACTIVE, UPDATE } from './mutation-types';
import { updateOrAddObjToArray } from '@/utils/helpers';

export default {
  [STORE](state, tournaments) {
    state.tournaments = tournaments || [];
  },
  [SET_ACTIVE](state, tournament) {
    state.activeTournament = tournament;
  },
  [UPDATE](state, tournament) {
    state.tournaments = updateOrAddObjToArray(state.tournaments, tournament);
  },
};
