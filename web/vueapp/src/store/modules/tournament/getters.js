/* ============
 * Getters for the tournament module
 * ============
 *
 * The getters that are available on the
 * tournament module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  tournamentIDLookup(state) {
    return generateIDLookup(state.tournaments);
  },

  tournaments(state, getters, rootState, rootGetters) {
    return state.tournaments.map(tournament => ({
      ...tournament,
      signedUpTeams: tournament.signedUpTeams.map(team => rootGetters['team/teamIDLookup'][team]),
      event: rootGetters['event/eventIDLookup'][tournament.eventID],
    }));
  },

  activeTournament(state, getters) {
    if (!state.activeTournament || !state.activeTournament.id) return undefined;
    return getters.tournaments.find(single => single.id === state.activeTournament.id);
  },
};
