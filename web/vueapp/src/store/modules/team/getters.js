/* ============
 * Getters for the team module
 * ============
 *
 * The getters that are available on the
 * team module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  teamsByTournament(state, getters) {
    /* eslint-disable arrow-body-style */
    return (tournamentID) => {
      return getters.teams.filter((single) => {
        return single.tournamentID === tournamentID;
      });
    };
    /* eslint-enable arrow-body-style */
  },

  teamsByUser(state, getters, rootState) {
    if (!rootState.account.email) return [];
    return getters.teams.filter(single => single.trainer
      && single.trainer.email
      && rootState.account.email === single.trainer.email);
  },

  teamIDLookup(state, getters) {
    return generateIDLookup(getters.teams);
  },

  teams(state, getters, rootState, rootGetters) {
    return state.teams.map(team => ({
      ...team,
      tournament: rootGetters['tournament/tournamentIDLookup'][team.tournamentID],
      players: team.players.map(player => rootGetters['player/playerIDLookup'][player]),
    }));
  },

  activeTeam(state, getters) {
    if (!state.activeTeam || !state.activeTeam.id) return undefined;
    return getters.teams.find(single => single.id === state.activeTeam.id);
  },
};
