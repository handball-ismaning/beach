/* ============
 * Getters for the player module
 * ============
 *
 * The getters that are available on the
 * player module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  playerIDLookup(state) {
    return generateIDLookup(state.players);
  },

  players(state, getters, rootState, rootGetters) {
    return state.players.map(player => ({
      ...player,
      team: rootGetters['team/teamIDLookup'][player.teamID],
    }));
  },
};
