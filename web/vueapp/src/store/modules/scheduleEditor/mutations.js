/* ============
 * Mutations for the ScheduleEditor module
 * ============
 *
 * The mutations that are available on the
 * ScheduleEditor module.
 */

import {
  ADD_GAME,
  ADD_GROUP,
  ADD_LEVEL,
  ADD_TEAM,
  CLEAN_DATA,
  DELETE_EMPTY_GAME,
  INSERT_EMPTY_GAME,
  MOVE_GAME_TO_INDEX,
  REMOVE_GAME_BY_GROUP,
  REMOVE_GAME_BY_LEVEL,
  REMOVE_GAME_BY_TEAM,
  REMOVE_GROUP,
  REMOVE_GROUP_BY_LEVEL,
  REMOVE_LEVEL,
  REMOVE_TEAM,
  REMOVE_TEAM_BY_GROUP,
  REMOVE_TEAM_BY_LEVEL,
  SET_GAMES,
  UPDATE_GAME_NUMBERS,
  UPDATE_GROUP,
  UPDATE_TEAM,
  SET_TEAMS,
  SET_LEVELS,
  SET_GROUPS,
} from './mutation-types';

import { uuid } from '@/utils/helpers';

function sortGames(state) {
  const sortedGames = state.games.sort((a, b) => a.number > b.number);
  state.games = sortedGames.map((single, index) => ({
    ...single,
    number: index + 1,
  }));
}

export default {
  [ADD_LEVEL](state, payload) {
    state.levels.push({
      ...payload,
      number: state.levels.filter(single =>
        single.tournamentID.toString() === payload.tournamentID.toString()).length + 1,
    });
  },
  [REMOVE_LEVEL](state, payload) {
    const level = state.levels.find(current => current.id === payload.levelID);
    const tournamentID = level ? level.tournamentID : undefined;
    state.levels = state.levels.filter(current => current.id !== payload.levelID);

    // Re-Number levels in the tournament
    if (tournamentID) {
      let number = 1;
      state.levels = state.levels.map((current) => {
        let result = current;
        if (current.tournamentID === tournamentID) {
          result = {
            ...current,
            number,
          };
          number += 1;
        }
        return result;
      });
    }
  },
  [ADD_GROUP](state, payload) {
    state.groups.push({
      id: uuid(),
      ...payload,
    });
  },
  [UPDATE_GROUP](state, payload) {
    state.groups = state.groups.map((current) => {
      if (current.id !== payload.groupID) return current;

      return {
        ...current,
        ...(payload.data),
      };
    });
  },
  [REMOVE_GROUP](state, payload) {
    state.groups = state.groups.filter(current => current.id !== payload.groupID);
  },
  [REMOVE_GROUP_BY_LEVEL](state, payload) {
    state.groups = state.groups.filter(current => current.levelID !== payload.levelID);
  },
  [ADD_TEAM](state, payload) {
    state.teams.push(payload);
  },
  [UPDATE_TEAM](state, payload) {
    state.teams = state.teams.map((current) => {
      if (current.id !== payload.teamID) return current;

      return {
        ...current,
        ...payload.data,
      };
    });
  },
  [REMOVE_TEAM](state, payload) {
    state.teams = state.teams.filter(current => current.id !== payload.teamID);
  },
  [REMOVE_TEAM_BY_LEVEL](state, payload) {
    state.teams = state.teams.filter(current => current.levelID !== payload.levelID);
  },
  [REMOVE_TEAM_BY_GROUP](state, payload) {
    state.teams = state.teams.filter(current => current.groupID !== payload.groupID);
  },
  [CLEAN_DATA](state) {
    state.levels = [];
    state.groups = [];
    state.teams = [];
    state.games = [];
  },
  [ADD_GAME](state, payload) {
    state.games.push({
      ...payload,
      number: state.games.length + 1,
    });
  },
  [UPDATE_GAME_NUMBERS](state) {
    sortGames(state);
  },
  [REMOVE_GAME_BY_TEAM](state, payload) {
    state.games = state.games.filter(current =>
      ((current.teamAID !== payload.teamID) && (current.teamBID !== payload.teamID)));
    sortGames(state);
  },
  [REMOVE_GAME_BY_GROUP](state, payload) {
    state.games = state.games.filter(current => current.groupID !== payload.groupID);
    sortGames(state);
  },
  [REMOVE_GAME_BY_LEVEL](state, payload) {
    state.games = state.games.filter(current => current.levelID !== payload.levelID);
    sortGames(state);
  },
  [MOVE_GAME_TO_INDEX](state, payload) {
    const { fromIndex, toIndex } = payload;
    const gamesLength = state.games.length;
    if (toIndex < 0 || fromIndex < 0 || fromIndex >= gamesLength || toIndex >= gamesLength) {
      return;
    }
    const game = state.games.splice(fromIndex, 1)[0];
    state.games.splice(toIndex, 0, game);
    state.games = state.games.map((current, index) => ({
      ...current,
      number: index + 1,
    }));
  },
  [INSERT_EMPTY_GAME](state, index) {
    state.games.splice(index, 0, {
      isEmpty: true,
      number: index,
    });
    sortGames(state);
  },
  [DELETE_EMPTY_GAME](state, index) {
    state.games.splice(index, 1);
    sortGames(state);
  },
  [SET_GAMES](state, payload) {
    state.games = payload;
    sortGames(state);
  },
  [SET_GROUPS](state, payload) {
    state.groups = payload;
  },
  [SET_TEAMS](state, payload) {
    state.teams = payload;
  },
  [SET_LEVELS](state, payload) {
    state.levels = payload;
  },
};
