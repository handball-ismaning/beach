/* ============
 * Actions for the ScheduleEditor module
 * ============
 *
 * The actions that are available on the
 * ScheduleEditor module.
 */

import * as types from './mutation-types';

import { uuid } from '@/utils/helpers';

export default {
  setData({ commit }, payload) {
    commit(types.CLEAN_DATA);

    const groupIDLookup = {};

    const levelLookup = {};
    const levels = [];

    const groups = payload.groups.map((group) => {
      const levelIndex = `${group.tournamentID}-${group.level}`;
      let currentLevel = levelLookup[levelIndex];
      if (!currentLevel) {
        const id = uuid();
        currentLevel = {
          tournamentID: group.tournamentID.toString(),
          number: group.level,
          id,
        };
        levelLookup[levelIndex] = currentLevel;
        levels.push(currentLevel);
      }
      group = {
        ...group,
        id: group.id.toString(),
        tournamentID: group.tournamentID.toString(),
        levelID: currentLevel.id.toString(),
      };
      groupIDLookup[group.id] = group;
      return group;
    });

    commit(types.SET_LEVELS, levels);
    commit(types.SET_GROUPS, groups);

    const teams = payload.teamReferences.map((team) => {
      const group = team.groupID ? groupIDLookup[team.groupID.toString()] : {};
      team = {
        id: team.id.toString(),
        groupID: group.id,
        levelID: group.levelID,
        tournamentID: group.tournamentID ? group.tournamentID.toString() : undefined,
        isAbsolute: team.isAbsolute,
        referenceTeamID: team.teamID ? team.teamID.toString() : undefined,
        place: team.place,
        referenceGroupID: team.fromGroupID ? team.fromGroupID.toString() : undefined,
        groupRelation: team.fromGroupID ? [group.levelID, team.fromGroupID.toString()] : [],
        games: undefined,
      };
      return team;
    });
    commit(types.SET_TEAMS, teams);

    const games = payload.games.map((game) => {
      const group = game.groupID ? groupIDLookup[game.groupID.toString()] : {};
      return {
        number: game.number,
        teamAID: game.teamAID ? game.teamAID.toString() : undefined,
        teamBID: game.teamBID ? game.teamBID.toString() : undefined,
        groupID: game.groupID ? game.groupID.toString() : undefined,
        tournamentID: group.tournamentID ? group.tournamentID.toString() : undefined,
        levelID: group.levelID ? group.levelID.toString() : undefined,
        isEmpty: game.isEmpty,
        isPlayed: game.isPlayed,
        scoreAHalftime1: game.scoreAHalftime1,
        scoreBHalftime1: game.scoreBHalftime1,
        scoreAHalftime2: game.scoreAHalftime2,
        scoreBHalftime2: game.scoreBHalftime2,
        scoreAHalftime3: game.scoreAHalftime3,
        scoreBHalftime3: game.scoreBHalftime3,
      };
    });

    commit(types.SET_GAMES, games);
  },

  addLevel({ commit }, payload) {
    commit(types.ADD_LEVEL, {
      id: uuid(),
      ...payload,
    });
  },

  removeLevel({ commit }, payload) {
    commit(types.REMOVE_LEVEL, payload);
    commit(types.REMOVE_GROUP_BY_LEVEL, payload);
    commit(types.REMOVE_TEAM_BY_LEVEL, payload);
    commit(types.REMOVE_GAME_BY_LEVEL, payload);
  },

  addGroup({ commit }, payload) {
    commit(types.ADD_GROUP, payload);
  },

  removeGroup({ commit }, payload) {
    commit(types.REMOVE_GROUP, payload);
    commit(types.REMOVE_TEAM_BY_GROUP, payload);
    commit(types.REMOVE_GAME_BY_GROUP, payload);
  },

  updateGroup({ commit }, payload) {
    commit(types.UPDATE_GROUP, payload);
  },

  addTeam({ commit, state, getters }, payload) {
    payload.id = payload.id || uuid();
    commit(types.ADD_TEAM, payload);

    const group = state.groups.find(single =>
      single.id.toString() === payload.groupID.toString());
    const teams = state.teams.filter(single =>
      single.groupID.toString() === payload.groupID.toString());
    if (teams.length > 1) {
      const otherTeams = teams.filter(single => single.id.toString() !== payload.id.toString());
      otherTeams.forEach((opponent) => {
        let teamA;
        let teamB;
        if (Math.random() >= 0.5) {
          teamA = payload;
          teamB = opponent;
        } else {
          teamA = opponent;
          teamB = payload;
        }

        commit(types.ADD_GAME, {
          teamAID: teamA.id.toString(),
          teamBID: teamB.id.toString(),
          groupID: group.id.toString(),
          tournamentID: group.tournamentID.toString(),
          levelID: group.levelID.toString(),
          isEmpty: false,
        });
      });
    }
  },

  updateTeam({ commit }, payload) {
    commit(types.UPDATE_TEAM, payload);
  },

  removeTeam({ commit }, payload) {
    commit(types.REMOVE_TEAM, payload);
    commit(types.REMOVE_GAME_BY_TEAM, payload);
  },

  moveGameUp({ commit }, payload) {
    commit(types.MOVE_GAME_TO_INDEX, {
      fromIndex: payload.number - 1,
      toIndex: payload.number - 2,
    });
  },

  moveGameDown({ commit }, payload) {
    commit(types.MOVE_GAME_TO_INDEX, {
      fromIndex: payload.number - 1,
      toIndex: payload.number,
    });
  },

  moveGameToIndex({ commit }, payload) {
    commit(types.MOVE_GAME_TO_INDEX, payload);
  },

  insertEmptyGame({ commit }, index) {
    commit(types.INSERT_EMPTY_GAME, index);
  },

  deleteEmptyGame({ commit }, index) {
    commit(types.DELETE_EMPTY_GAME, index);
  },

  setGames({ commit }, payload) {
    commit(types.SET_GAMES, payload);
  },
};
