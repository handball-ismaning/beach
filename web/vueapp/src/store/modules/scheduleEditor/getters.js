/* ============
 * Getters for the ScheduleEditor module
 * ============
 *
 * The getters that are available on the
 * ScheduleEditor module.
 */

/* eslint-disable arrow-body-style */

import Vue from 'vue';
import moment from 'moment';

import { combineDateTime } from '@/utils/helpers';

function getTeamName(teamReference, teamLookup, groupsLookup) {
  if (teamReference.isAbsolute && teamReference.referenceTeamID) {
    const team = teamLookup[teamReference.referenceTeamID];
    if (!team) return undefined;
    return team.completeName;
  } else if (!teamReference.isAbsolute && teamReference.referenceGroupID && teamReference.place) {
    const group = groupsLookup[teamReference.referenceGroupID];
    if (!group) return undefined;
    return `${teamReference.place}. ${group.name}`;
  }
  return undefined;
}

function calculateCourtForGame(game, event) {
  return (((game.number - 1) + event.courtCount) % event.courtCount) + 1;
}

function calculateDatetimeforGame(game, event) {
  const startDate = combineDateTime(event.startDate, event.startTime);
  const date = moment(startDate);

  const gameOffset = Math.floor((game.number - 1) / event.courtCount);
  const minutesPerDay = combineDateTime(event.startDate, event.endTime).diff(startDate, 'minutes');
  const gamesPerDay = Math.ceil(minutesPerDay / event.totalGameDuration);
  const dayOffset = Math.floor(gameOffset / gamesPerDay);
  const gamesOffsetAtDay = (gameOffset - (dayOffset * gamesPerDay)) % gamesPerDay;

  date.add(dayOffset, 'days');
  date.add(gamesOffsetAtDay * event.totalGameDuration, 'minutes');

  return date;
}

export default {
  levelsByTournament(state, getters, rootState) {
    const activeTournament = rootState.tournament.activeTournament;
    if (!activeTournament) return [];
    return state.levels.filter(current =>
      current.tournamentID.toString() === activeTournament.id.toString())
      .sort((a, b) => (a.number > b.number ? 1 : -1));
  },
  groupsByLevel(state) {
    return (levelID) => {
      return state.groups.filter((single) => {
        return single.levelID.toString() === levelID.toString();
      });
    };
  },
  teamsByGroup(state) {
    return (groupID) => {
      return state.teams.filter((single) => {
        return single.groupID.toString() === groupID.toString();
      });
    };
  },
  groupsAvailableInLevel(state, getters) {
    return (levelID) => {
      const level = getters.levelsByTournament
        .find(single => single.id.toString() === levelID.toString());
      if (!level) return [];

      const levelsBefore = getters.levelsByTournament.filter(single =>
        single.number < level.number);
      if (levelsBefore.length === 0) return [];

      return levelsBefore.map(single => ({
        value: single.id.toString(),
        label: Vue.i18n.t('schedule.edit.level_nr', { nr: single.number }),
        children: state.groups
          .filter(current => current.levelID.toString() === single.id.toString())
          .map(current => ({
            value: current.id.toString(),
            label: current.name,
          })),
      }));
    };
  },

  games(state, getters, rootState, rootGetters) {
    const tournamentLookup = rootGetters['tournament/tournamentIDLookup'];
    const teamLookup = rootGetters['team/teamIDLookup'];
    const event = rootGetters['event/activeEvent'];

    const groupsLookup = state.groups.reduce((result, single) => {
      result[single.id] = single;
      return result;
    }, {});

    const teamReferenceLookup = state.teams.reduce((result, single) => {
      result[single.id] = {
        ...single,
        name: getTeamName(single, teamLookup, groupsLookup),
      };
      return result;
    }, {});

    return state.games.map(game => ({
      ...game,
      tournament: game.tournamentID ? tournamentLookup[game.tournamentID] : undefined,
      teamA: game.teamAID ? teamReferenceLookup[game.teamAID] : undefined,
      teamB: game.teamBID ? teamReferenceLookup[game.teamBID] : undefined,
      number: game.number,
      court: calculateCourtForGame(game, event),
      datetime: calculateDatetimeforGame(game, event),
      group: game.groupID ? {
        ...groupsLookup[game.groupID],
        tournament: game.tournamentID ? tournamentLookup[game.tournamentID] : undefined,
      } : undefined,
    }));
  },
};
