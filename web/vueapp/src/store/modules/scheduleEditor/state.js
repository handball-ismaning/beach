/* ============
 * State of the ScheduleEditor module
 * ============
 *
 * The initial state of the ScheduleEditor module.
 */

export default {
  levels: [],
  groups: [],
  teams: [],
  games: [],
};
