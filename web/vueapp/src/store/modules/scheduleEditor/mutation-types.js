/* ============
 * Mutation types for the ScheduleEditor module
 * ============
 *
 * The mutation types that are available
 * on the ScheduleEditor module.
 */

export const ADD_LEVEL = 'ADD_LEVEL';
export const REMOVE_LEVEL = 'REMOVE_LEVEL';
export const ADD_GROUP = 'ADD_GROUP';
export const UPDATE_GROUP = 'UPDATE_GROUP';
export const REMOVE_GROUP = 'REMOVE_GROUP';
export const REMOVE_GROUP_BY_LEVEL = 'REMOVE_GROUP_BY_LEVEL';
export const ADD_TEAM = 'ADD_TEAM';
export const UPDATE_TEAM = 'UPDATE_TEAM';
export const REMOVE_TEAM = 'REMOVE_TEAM';
export const REMOVE_TEAM_BY_LEVEL = 'REMOVE_TEAM_BY_LEVEL';
export const REMOVE_TEAM_BY_GROUP = 'REMOVE_TEAM_BY_GROUP';
export const CLEAN_DATA = 'CLEAN_DATA';
export const ADD_GAME = 'ADD_GAME';
export const REMOVE_GAME_BY_TEAM = 'REMOVE_GAME_BY_TEAM';
export const REMOVE_GAME_BY_GROUP = 'REMOVE_GAME_BY_GROUP';
export const REMOVE_GAME_BY_LEVEL = 'REMOVE_GAME_BY_LEVEL';
export const MOVE_GAME_TO_INDEX = 'MOVE_GAME_TO_INDEX';
export const UPDATE_GAME_NUMBERS = 'UPDATE_GAME_NUMBERS';
export const INSERT_EMPTY_GAME = 'INSERT_EMPTY_GAME';
export const DELETE_EMPTY_GAME = 'DELETE_EMPTY_GAME';
export const SET_GAMES = 'SET_GAMES';
export const SET_GROUPS = 'SET_GROUPS';
export const SET_TEAMS = 'SET_TEAMS';
export const SET_LEVELS = 'SET_LEVELS';

export default {
  ADD_LEVEL,
  REMOVE_LEVEL,
  ADD_GROUP,
  UPDATE_GROUP,
  REMOVE_GROUP,
  ADD_TEAM,
  UPDATE_TEAM,
  REMOVE_TEAM,
  REMOVE_TEAM_BY_LEVEL,
  REMOVE_TEAM_BY_GROUP,
  REMOVE_GROUP_BY_LEVEL,
  CLEAN_DATA,
  ADD_GAME,
  REMOVE_GAME_BY_TEAM,
  REMOVE_GAME_BY_GROUP,
  REMOVE_GAME_BY_LEVEL,
  MOVE_GAME_TO_INDEX,
  UPDATE_GAME_NUMBERS,
  INSERT_EMPTY_GAME,
  DELETE_EMPTY_GAME,
  SET_GAMES,
  SET_GROUPS,
  SET_TEAMS,
  SET_LEVELS,
};
