/* ============
 * Actions for the team  reference module
 * ============
 *
 * The actions that are available on the
 * team reference module.
 */

import * as types from './mutation-types';

export const store = ({ commit }, payload) => {
  commit(types.STORE, payload);
};

export default {
  store,
};
