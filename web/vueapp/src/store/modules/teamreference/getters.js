/* ============
 * Getters for the team reference module
 * ============
 *
 * The getters that are available on the
 * team reference module.
 */

import { generateIDLookup } from '@/utils/helpers';

export default {
  teamreferenceIDLookup(state, getters) {
    return generateIDLookup(getters.teamreferences);
  },

  teamreferences(state, getters, rootState, rootGetters) {
    return state.teamreferences.map(team => ({
      ...team,
      group: rootGetters['group/groupIDLookup'][team.groupID],
      fromGroup: rootGetters['group/groupIDLookup'][team.fromGroupID],
      team: rootGetters['team/teamIDLookup'][team.teamID],
    }));
  },
};
