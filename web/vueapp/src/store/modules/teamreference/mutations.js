/* ============
 * Mutations for the team reference module
 * ============
 *
 * The mutations that are available on the
 * team reference module.
 */

import { STORE } from './mutation-types';

export default {
  [STORE](state, teams) {
    state.teamreferences = teams || [];
  },
};
