/* ============
 * State of the team reference module
 * ============
 *
 * The initial state of the team reference module.
 */

export default {
  teamreferences: [],
};
