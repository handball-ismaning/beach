/* ============
 * Mutation types for the team reference module
 * ============
 *
 * The mutation types that are available
 * on the team reference module.
 */

export const STORE = 'STORE';

export default {
  STORE,
};
