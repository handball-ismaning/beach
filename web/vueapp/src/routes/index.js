/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

import tournamentService from '@/services/tournament';
import teamService from '@/services/team';
import playerService from '@/services/player';
import eventService from '@/services/events';
import store from '@/store';
import moment from 'moment';
import Vue from 'vue';
import gameService from '@/services/games';
import groupService from '@/services/group';
import teamReferenceService from '@/services/teamreference';
import scheduleEditorService from '@/services/scheduleEditor';

/**
 * The routes
 *
 * @type {object} The routes
 */
export default [
  // Home
  {
    path: '/home',
    name: 'home.index',
    component: require('@/pages/home/index.vue'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: false,
    },

    beforeEnter: (to, from, next) => {
      eventService.getAll().then(next);
    },
  },

  // Registration
  {
    path: '/registration',
    component: require('@/pages/etc/emptyRouterView.vue'),
    children: [{
      path: '',
      name: 'registration.index',
      component: require('@/pages/registration/index.vue'),

      // If the user needs to be authenticated to view this page
      meta: {
        auth: false,
      },

      beforeEnter: (to, from, next) => {
        Promise.all([
          tournamentService.getAll(),
          teamService.getMine(),
        ]).catch(next).then(next);
      },
    }, {
      path: 'register-team',
      name: 'team.register',
      component: require('@/pages/team/signup.vue'),

      // If the user needs to be a authenticated to view this page
      meta: {
        auth: false,
      },

      beforeEnter: (to, from, next) => {
        Promise.all([
          tournamentService.getAll(),
          tournamentService.setCurrentSignupTournament(to.query.tournament),
        ]).catch(next).then(next);
      },
    },

    // Tournament Single Page
    {
      path: 'tournament/:tournamentID',
      component: require('@/pages/tournament/wrapper.vue'),
      // If the user needs to be a authenticated to view this page
      meta: {
        auth: false,
      },

      beforeEnter: (to, from, next) => {
        Promise.all([
          tournamentService.getByID(to.params.tournamentID),
          teamService.getAll(),
        ]).then(next);
      },

      children: [
        {
          path: '',
          name: 'tournament.single',
          component: require('@/pages/tournament/single.vue'),
        }, {
          // Team Single Page
          path: 'team/:teamID',
          component: require('@/pages/team/wrapper.vue'),

          beforeEnter: (to, from, next) => {
            if (teamService.checkIsAllowedToViewTeam(to.params.teamID)) {
              return Promise.all([
                teamService.getByID(to.params.teamID),
                playerService.getByTeam({ id: to.params.teamID }),
              ]).then(next);
            }
            return next(new Error());
          },

          auth: true,

          children: [
            {
              path: '',
              name: 'team.single',
              component: require('@/pages/team/single.vue'),
            }, {
              path: 'edit',
              name: 'team.edit',
              component: require('@/pages/team/edit.vue'),

              beforeEnter: (to, from, next) => {
                if (store.state.account.isStaff) return next();
                const currentTournament = store.getters['tournament/activeTournament'] || {};
                if (moment().isBefore(currentTournament.deadlineEdit)) {
                  return next();
                }
                Vue.$notify.error({
                  title: Vue.i18n.t('team.notifications.put.after_deadline_edit.title'),
                  message: Vue.i18n.t('team.notifications.put.after_deadline_edit.message'),
                });
                return next(new Error());
              },
            },
          ],
        }],
    }],
  },

  // Account
  {
    path: '/account',
    name: 'auth.account',
    component: require('@/pages/auth/account.vue'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
    },

    beforeEnter: (to, from, next) => {
      Promise.all([
        teamService.getMine(),
        playerService.getMine(),
      ]).then(next);
    },
  },

  // Login
  {
    path: '/login',
    name: 'auth.login',
    component: require('@/pages/auth/login.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  // Password Forgot
  {
    path: '/password/forgot',
    name: 'auth.password-forgot',
    component: require('@/pages/auth/password-forgot.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  // Password Reset
  {
    path: '/password/reset',
    name: 'auth.password-reset',
    component: require('@/pages/auth/password-reset.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  // Register
  {
    path: '/signup',
    name: 'auth.registration',
    component: require('@/pages/auth/registration.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  // Email Verify
  {
    path: '/signup/verify',
    name: 'auth.email-verify',
    component: require('@/pages/auth/email-verify.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: false,
    },
  },

  // ComingSoon Page
  {
    path: '/coming-soon',
    name: 'etc.comingSoon',
    component: require('@/pages/etc/comingSoon.vue'),

    // If the user needs to be a authenticated to view this page
    meta: {
      auth: false,
    },
  },

  {
    path: '/contact',
    name: 'etc.contact',
    component: require('@/pages/etc/contact.vue'),

    // If the user needs to be a authenticated to view this page
    meta: {
      auth: false,
    },
  },

  {
    path: '/privacy',
    name: 'etc.privacy',
    component: require('@/pages/etc/privacy.vue'),

    // If the user needs to be a authenticated to view this page
    meta: {
      auth: false,
    },
  },

  {
    path: '/terms-of-participation',
    name: 'etc.terms-of-participation',
    component: require('@/pages/etc/termsOfParticipation.vue'),

    // If the user needs to be a authenticated to view this page
    meta: {
      auth: false,
    },
  },

  {
    path: '/manage',
    name: 'etc.admin',
    component: require('@/pages/etc/admin.vue'),

    // If the user needs to be a authenticated to view this page
    meta: {
      auth: true,
    },

    beforeEnter: (to, from, next) => {
      Promise.all([
        tournamentService.getAll(),
        teamService.getAll(),
        playerService.getAll(),
      ]).then(next);
    },
  },

  {
    path: '/schedule',
    component: require('@/pages/etc/emptyRouterView.vue'),
    beforeEnter: (to, from, next) => {
      Promise.all([
        tournamentService.getAll(),
        groupService.getAll(),
        teamService.getAll(),
        teamReferenceService.getAll(),
      ]).then(next);
    },

    children: [
      {
        path: 'event/:eventID',
        component: require('@/pages/schedule/wrapper.vue'),
        beforeEnter: (to, from, next) => {
          eventService.getByID(to.params.eventID).then(() => {
            if (store.state.account.isStaff) return next();
            if (to.name === 'schedule.not-available') return next();
            const activeEvent = store.getters['event/activeEvent'];
            if (activeEvent && !activeEvent.schedulePublic) {
              return next({ name: 'schedule.not-available', params: to.params });
            }
            return next();
          });
        },

        children: [{
          path: '',
          name: 'schedule.event',
          component: require('@/pages/schedule/event.vue'),
          beforeEnter: (to, from, next) => {
            gameService.getByEvent(to.params.eventID).then(next);
          },
        }, {
          path: 'edit',
          name: 'schedule.edit',
          component: require('@/pages/schedule/edit.vue'),
          beforeEnter: (to, from, next) => {
            scheduleEditorService.getGroupStructure(to.params.eventID).then(next);
          },
        }, {
          path: 'tournament/:tournamentID',
          component: require('@/pages/etc/emptyRouterView.vue'),
          beforeEnter: (to, from, next) => {
            tournamentService.getByID(to.params.tournamentID).then(next);
          },
          children: [{
            path: '',
            name: 'schedule.tournament',
            component: require('@/pages/schedule/tournament.vue'),
            beforeEnter: (to, from, next) => {
              gameService.getByTournament(to.params.tournamentID).then(next);
            },
          }, {
            path: 'group/:groupID',
            name: 'schedule.group',
            component: require('@/pages/schedule/group.vue'),
            beforeEnter: (to, from, next) => {
              Promise.all([
                gameService.getByGroup(to.params.groupID),
                groupService.getByID(to.params.groupID),
              ]).then(next);
            },
          }],
        }, {
          path: 'date/:date',
          name: 'schedule.date',
          component: require('@/pages/schedule/date.vue'),
          beforeEnter: (to, from, next) => {
            gameService.getByDate(to.params.date).then(next);
          },
        }, {
          path: 'court/:court',
          name: 'schedule.court',
          component: require('@/pages/schedule/court.vue'),
          beforeEnter: (to, from, next) => {
            gameService.getByCourt(to.params.court).then(next);
          },
        }, {
          path: 'team/:teamID',
          name: 'schedule.team',
          component: require('@/pages/schedule/team.vue'),
          beforeEnter: (to, from, next) => {
            Promise.all([
              gameService.getByTeam(to.params.teamID),
              teamService.getByID(to.params.teamID),
            ]).then(next);
          },
        }, {
          path: 'games/:gameID',
          component: require('@/pages/etc/emptyRouterView.vue'),
          beforeEnter: (to, from, next) => {
            gameService.getByID(to.params.gameID).then(next);
          },
          children: [{
            path: '',
            name: 'games.single',
            component: require('@/pages/game/single.vue'),
          }, {
            path: 'edit',
            name: 'games.edit',
            component: require('@/pages/game/edit.vue'),

            beforeEnter: (to, from, next) => {
              const currentGame = store.getters['games/activeGame'] || {};
              if (currentGame.isEmpty) {
                return next(new Error());
              }
              if (store.state.account.isStaff) return next();
              return next(new Error());
            },
          }],
        }, {
          path: 'not-available',
          name: 'schedule.not-available',
          component: require('@/pages/schedule/notavailable.vue'),
        }],
      },
      {
        path: 'print-games/:eventID',
        name: 'games.print',
        component: require('@/pages/game/print.vue'),
        beforeEnter: (to, from, next) => {
          Promise.all([
            gameService.getByEvent(to.params.eventID),
            eventService.getByID(to.params.eventID),
          ]).then(next);
        },
      },
      {
        path: 'print/:eventID',
        name: 'schedule.print',
        component: require('@/pages/schedule/print.vue'),
        beforeEnter: (to, from, next) => {
          Promise.all([
            eventService.getByID(to.params.eventID),
            tournamentService.getAll(),
            teamService.getAll(),
            teamReferenceService.getAll(),
            groupService.getAll(),
            gameService.getByEvent(to.params.eventID),
          ]).then(next);
        },
      },
    ],
  },

  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/*',
    redirect: '/home',
  },
];
