/* ============
 * Main File
 * ============
 *
 * Will initialize the application.
 */

import Vue from 'vue';
import * as App from './app';

require('./bootstrap');

import configService from '@/services/config';
import eventService from '@/services/events';

Promise.all([
  configService.getConfig(),
  eventService.getAll(),
]).then(() => {
  new Vue(App).$mount('#app');
});
