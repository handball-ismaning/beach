import getAll from './getAll';
import getByID from './getByID';

export default {
  getAll,
  getByID,
};
