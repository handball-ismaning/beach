import eventTransformer from './../../transformers/event';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (event) => {
  event = eventTransformer.fetch(event);

  store.dispatch('event/setActive', event);
};

// When the request fails
const failed = () => {
  Vue.router.replace({
    name: 'home.index',
  });
  Vue.$notify.error({
    title: Vue.i18n.t('event.notifications.get.failed.title'),
    message: Vue.i18n.t('event.notifications.get.failed.message'),
  });
};

export default eventID =>
  Vue.$http.get(`/events/${eventID}/`)
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
