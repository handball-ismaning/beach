import eventTransformer from './../../transformers/event';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (events) => {
  events = eventTransformer.fetchCollection(events);

  store.dispatch('event/store', events);
};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('event.notifications.get.failed.title'),
    message: Vue.i18n.t('event.notifications.get.failed.message'),
  });
};

export default () =>
  Vue.$http.get('/events/')
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
