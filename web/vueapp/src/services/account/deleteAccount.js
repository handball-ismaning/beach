import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = () => {
  store.dispatch('auth/logout');
  Vue.router.replace({
    name: 'registration.index',
  });
  Vue.$notify.success({
    title: Vue.i18n.t('account.notifications.delete.success.title'),
    message: Vue.i18n.t('account.notifications.delete.success.message'),
  });
};

// When the request fails
const failed = (error) => {
  if (error && error.response && error.response.data && error.response.data.key === 'no_password_given') {
    Vue.$notify.error({
      title: Vue.i18n.t('account.notifications.delete.no_password_given.title'),
      message: Vue.i18n.t('account.notifications.delete.no_password_given.message'),
    });
  } else if (error && error.response && error.response.data && error.response.data.key === 'invalid_password') {
    Vue.$notify.error({
      title: Vue.i18n.t('account.notifications.delete.invalid_password.title'),
      message: Vue.i18n.t('account.notifications.delete.invalid_password.message'),
    });
  } else if (error && error.response && error.response.data && error.response.data.key === 'has_signed_up_teams') {
    Vue.$notify.error({
      title: Vue.i18n.t('account.notifications.delete.has_signed_up_teams.title'),
      message: Vue.i18n.t('account.notifications.delete.has_signed_up_teams.message'),
    });
  } else {
    Vue.$notify.error({
      title: Vue.i18n.t('account.notifications.delete.failed.title'),
      message: Vue.i18n.t('account.notifications.delete.failed.message'),
    });
  }
};

export default password =>
  /*
   * Normally you would perform an AJAX-request.
   * But to get the example working, the data is hardcoded.
   *
   * With the include REST-client Axios, you can do something like this:
   * Vue.$http.get('/account')
   *   .then((response) => {
   *     success(response);
   *   })
   *   .catch((error) => {
   *     failed(error);
   *   });
   */
  Vue.$http.post('/account/delete/', { password })
    .then((response) => {
      success(response.data);
      return Promise.resolve();
    }).catch((error) => {
      failed(error);
      return Promise.reject(error);
    });
