import find from './find';
import update from './update';
import deleteAccount from './deleteAccount';

export default {
  find,
  update,
  deleteAccount,
};
