import groupTransformer from './../../transformers/group';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (group) => {
  group = groupTransformer.fetch(group);

  store.dispatch('group/setActive', group);
};

// When the request fails
const failed = () => {
  Vue.router.replace({
    name: 'home.index',
  });
  Vue.$notify.error({
    title: Vue.i18n.t('group.notifications.get.failed.title'),
    message: Vue.i18n.t('group.notifications.get.failed.message'),
  });
};

export default groupID =>
  Vue.$http.get(`/groups/${groupID}/`)
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
