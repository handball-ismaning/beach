import groupTransformer from './../../transformers/group';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (groups) => {
  groups = groupTransformer.fetchCollection(groups);

  store.dispatch('group/store', groups);
};

// When the request fails
const failed = () => {
  Vue.router.replace({
    name: 'home.index',
  });
  Vue.$notify.error({
    title: Vue.i18n.t('group.notifications.get.failed.title'),
    message: Vue.i18n.t('group.notifications.get.failed.message'),
  });
};

export default tournamentID =>
  Vue.$http.get(`/groups/?tournament=${tournamentID}`)
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
