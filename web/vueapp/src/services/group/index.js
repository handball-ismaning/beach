import getByID from './getByID';
import getByTournament from './getByTournament';
import getAll from './getAll';

export default {
  getByID,
  getByTournament,
  getAll,
};
