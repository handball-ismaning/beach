import gameTransformer from './../../transformers/game';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (game) => {
  game = gameTransformer.fetch(game);

  store.dispatch('games/updateOrAdd', game);
  store.dispatch('games/setActive', game);
};

// When the request fails
const failed = () => {
  Vue.router.replace({
    name: 'home.index',
  });
  Vue.$notify.error({
    title: Vue.i18n.t('game.notifications.get.failed.title'),
    message: Vue.i18n.t('game.notifications.get.failed.message'),
  });
};

export default id =>
  Vue.$http.get(`/games/${id}/`)
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
