import gameTransformer from './../../transformers/game';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (game) => {
  game = gameTransformer.fetch(game);

  store.dispatch('games/updateSingle', game);
};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('game.notifications.put.failed.title'),
    message: Vue.i18n.t('game.notifications.put.failed.message'),
  });
};

export default (game) => {
  const gameID = game.id;
  return Vue.$http.put(`/games/${gameID}/update_referees/`, gameTransformer.send(game))
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
};
