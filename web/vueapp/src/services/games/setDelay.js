import Vue from 'vue';

// When the request succeeds
const success = () => {};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('game.notifications.put.failed.title'),
    message: Vue.i18n.t('game.notifications.put.failed.message'),
  });
};

export default (game, delay) => {
  const gameID = game.id;
  return Vue.$http.post(`/games/${gameID}/set_delay/`, { delay })
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
};
