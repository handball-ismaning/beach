import getByEvent from './getByEvent';
import getByTournament from './getByTournament';
import getByGroup from './getByGroup';
import getByDate from './getByDate';
import getByCourt from './getByCourt';
import getByTeam from './getByTeam';
import getByID from './getByID';
import update from './update';
import setDelay from './setDelay';
import updateReferees from './updateReferees';

export default {
  getByEvent,
  getByTournament,
  getByGroup,
  getByDate,
  getByCourt,
  getByTeam,
  getByID,
  update,
  setDelay,
  updateReferees,
};
