import gameTransformer from './../../transformers/game';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (games) => {
  games = gameTransformer.fetchCollection(games);

  store.dispatch('games/store', games);
};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('game.notifications.get.failed.title'),
    message: Vue.i18n.t('game.notifications.get.failed.message'),
  });
};

export default date =>
  Vue.$http.get(`/games/?date=${date}`)
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
