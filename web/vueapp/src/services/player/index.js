import getAll from './getAll';
import getByTeam from './getByTeam';
import getMine from './getMine';

export default {
  getAll,
  getByTeam,
  getMine,
};
