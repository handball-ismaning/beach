import Vue from 'vue';
import store from './../../store';
import TournamentTransformer from './../../transformers/tournament';
import GroupTransformer from '../../transformers/group';
import TeamReferenceTransformer from '../../transformers/teamReference';
import GameTransformer from '../../transformers/game';

// When the request succeeds
const success = (data) => {
  data.tournaments = TournamentTransformer.fetchCollection(data.tournaments);
  data.groups = GroupTransformer.fetchCollection(data.groups);
  data.teamReferences = TeamReferenceTransformer.fetchCollection(data.team_references);
  data.games = GameTransformer.fetchCollection(data.games);

  store.dispatch('tournament/store', data.tournaments);
  store.dispatch('scheduleEditor/setData', data);
};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('schedule.edit.notifications.get.failed.title'),
    message: Vue.i18n.t('schedule.edit.notifications.get.failed.message'),
  });
};

export default eventID =>
  Vue.$http.get(`/schedule/group_structure/${eventID}/`)
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
      throw error;
    });
