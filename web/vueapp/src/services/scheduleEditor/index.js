import getGroupStructure from './getGroupStructure';
import updateGroupStructure from './updateGroupStructure';

export default {
  getGroupStructure,
  updateGroupStructure,
};
