import Vue from 'vue';

// When the request succeeds
const success = () => {
  Vue.$notify.success({
    title: Vue.i18n.t('schedule.edit.notifications.put.success.title'),
    message: Vue.i18n.t('schedule.edit.notifications.put.success.message'),
  });
};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('schedule.edit.notifications.put.failed.title'),
    message: Vue.i18n.t('schedule.edit.notifications.put.failed.message'),
  });
};

export default (eventID, store) => {
  const data = {};

  const levelsByID = store.state.scheduleEditor.levels.reduce((result, single) => {
    result[single.id] = single.number;
    return result;
  }, {});

  data.groups = store.state.scheduleEditor.groups.map(single => ({
    id: single.id,
    name: single.name,
    tournament_id: single.tournamentID,
    level: levelsByID[single.levelID],
  }));

  data.teams = store.state.scheduleEditor.teams.map(single => ({
    id: single.id,
    tournament_id: single.tournamentID,
    group_id: single.groupID,
    is_absolute: single.isAbsolute,
    from_group_id: single.referenceGroupID,
    team_id: single.referenceTeamID,
    place: single.place,
  }));

  data.games = store.getters['scheduleEditor/games'].map(single => ({
    group_id: single.groupID,
    team_a_id: single.teamAID,
    team_b_id: single.teamBID,
    number: single.number,
    court: single.court,
    datetime: single.datetime.toISOString(),
    scoreAHalftime1: single.scoreAHalftime1,
    scoreBHalftime1: single.scoreBHalftime1,
    scoreAHalftime2: single.scoreAHalftime2,
    scoreBHalftime2: single.scoreBHalftime2,
    scoreAHalftime3: single.scoreAHalftime3,
    scoreBHalftime3: single.scoreBHalftime3,
  }));

  return Vue.$http.post(`/schedule/group_structure/${eventID}/`, data)
    .then(() => {
      success();
    }).catch((error) => {
      failed(error);
    });
};
