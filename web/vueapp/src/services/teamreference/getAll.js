import teamReferenceTransformer from './../../transformers/teamReference';
import store from './../../store';
import Vue from 'vue';

// When the request succeeds
const success = (teams) => {
  teams = teamReferenceTransformer.fetchCollection(teams);

  store.dispatch('teamreference/store', teams);
};

// When the request fails
const failed = () => {
  Vue.$notify.error({
    title: Vue.i18n.t('team.notifications.get.failed.title'),
    message: Vue.i18n.t('team.notifications.get.failed.message'),
  });
};

export default () =>
  Vue.$http.get('/teamreferences/')
    .then((response) => {
      success(response.data);
    }).catch((error) => {
      failed(error);
    });
