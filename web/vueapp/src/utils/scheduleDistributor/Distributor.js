import hash from 'hash.js';

import moment from 'moment';
import { combineDateTime, uuidFromNumber, getTeamHash } from '@/utils/helpers';

function getEndTime(tournaments) {
  return tournaments.reduce((result, tournament) => {
    if (!result) return tournament.endDateGames;

    if (tournament.endDateGames.isAfter(result)) {
      result = tournament.endDateGames;
    }
    return result;
  }, undefined);
}

function getGamesByTournament(games) {
  return games.reduce((obj, game) => {
    if (!game.tournamentID) return obj;
    if (obj[game.tournamentID.toString()]) {
      obj[game.tournamentID.toString()].push(game);
    } else {
      obj[game.tournamentID.toString()] = [game];
    }
    return obj;
  }, {});
}

function getCurrentLevels(tournaments, games) {
  const gamesByTournament = getGamesByTournament(games);
  return tournaments.reduce((obj, tournament) => {
    const tournamentGames = gamesByTournament[tournament.id.toString()];
    if (!tournamentGames || tournamentGames.length === 0) return obj;
    const levels = tournamentGames.map(game => parseInt(game.group.level, 10));
    const min = levels.reduce((acc, current) => {
      if (!acc) return current;
      return Math.min(acc, current);
    }, undefined);
    obj[tournament.id.toString()] = Math.max(min, 1);
    return obj;
  }, {});
}

function getTournamentLevelLookup(tournaments, games) {
  return tournaments.reduce((obj, tournament) => {
    obj[tournament.id.toString()] = {
      ...tournament,
      levels: games
        .filter(game =>
          game.tournamentID && game.tournamentID.toString() === tournament.id.toString())
        .reduce((acc, game) => {
          if (acc[game.group.level.toString()]) {
            acc[game.group.level.toString()].push(game);
          } else {
            acc[game.group.level.toString()] = [game];
          }
          return acc;
        }, {}),
    };
    return obj;
  }, {});
}

function increaseDateTime(datetime, event) {
  let current = moment(datetime);
  const end = combineDateTime(current, event.endTime);
  if (end.diff(current, 'minutes') > event.totalGameDuration) {
    current.add(event.totalGameDuration, 'minutes');
  } else {
    current.add(1, 'day');
    current = combineDateTime(current, event.startTime);
  }
  return current;
}

export default class Distributor {
  constructor(vm) {
    this.vm = vm;
  }

  createHashForGameList(games) {
    const gameHashes = games.filter(current => !current.isEmpty).map((game) => {
      const teamHashes = [getTeamHash(game.teamA), getTeamHash(game.teamB)].sort();
      return hash.sha256().update(`${game.groupID}-${teamHashes.join('-')}`).digest('hex');
    });
    return hash.sha256().update(gameHashes.sort()).digest('hex');
  }

  checkHashUnchanged(oldHash, games) {
    if (oldHash !== this.createHashForGameList(games)) {
      throw new Error('Did not retain games correctly');
    }
  }

  /**
   * Distribute the games across the schedule,
   * while maintaining tournament & group structure
   * @param   {Array}  games
   * @param   {Array}  tournaments
   * @param   {Object} event
   * @returns {Array} newly sorted games
   */
  distributeGames(games, tournaments, event) {
    if (!games || games.length === 0) return [];

    const hashBefore = this.createHashForGameList(games);

    const result = [];

    games = [...(games.filter(game => !game.isEmpty))].map(current => ({
      ...current,
      id: uuidFromNumber(current.number),
      teamAHash: getTeamHash(current.teamA),
      teamBHash: getTeamHash(current.teamB),
    }));

    let currentDateTime = combineDateTime(event.startDate, event.startTime);
    const endTime = getEndTime(tournaments);
    const tournamentLevelLookup = getTournamentLevelLookup(tournaments, games);

    this.beforeHandler(games, tournaments);

    while (games.length > 0) {
      this.checkLastGamesEmpty(result, event, currentDateTime, endTime);
      const currentLevels = getCurrentLevels(tournaments, games);
      let availableGames = this.getAvailableGames(
        games,
        tournaments,
        currentDateTime,
        tournamentLevelLookup,
        currentLevels,
      );
      availableGames = this.sortAvailableGames(availableGames);

      this.beforeGameAddHandler(availableGames);

      for (let i = 0; i < event.courtCount; i += 1) {
        let game = {
          number: result.length + 1,
          isEmpty: true,
        };
        availableGames = this.filterAvailableGames(availableGames, result);

        if (availableGames.length > 0) {
          game = {
            ...this.getGameFromAvailableGames(availableGames),
            ...game,
            isEmpty: false,
          };
          games = games.filter(current => current.id.toString() !== game.id.toString());
          availableGames = availableGames.filter(current =>
            current.id.toString() !== game.id.toString());
          this.nonEmptyGameAddHandler(game);
        }
        result.push(game);
      }
      currentDateTime = increaseDateTime(currentDateTime, event);
    }

    this.checkHashUnchanged(hashBefore, result);

    return result;
  }

  checkLastGamesEmpty(result, event, currentDateTime, endTime) {
    const lastGames = result.slice(result.length - event.courtCount);
    const lastGamesEmpty = lastGames.reduce((acc, g) => {
      if (!g.isEmpty) return false;
      return acc;
    }, true);
    if (currentDateTime.isAfter(endTime) && lastGamesEmpty) {
      throw new Error('Unable to generate Games');
    }
  }

  getAvailableGames(games, tournaments, datetime, tournamentLevelLookup, currentLevels) {
    const availableTournamentIDs = this.getAvailableTournaments(tournaments, datetime)
      .map(tournament => tournament.id.toString());

    const gameIDs = games.map(current => current.id);

    return games.reduce((result, game) => {
      if (!game.tournamentID || !availableTournamentIDs.includes(game.tournamentID.toString())) {
        return result;
      }

      const tournament = tournamentLevelLookup[game.tournamentID.toString()];
      if (!tournament) return result;
      const currentLevel = currentLevels[game.tournamentID.toString()];
      if (!currentLevel) return result;
      let tournamentLevelGames = tournament.levels[currentLevel];
      if (!tournamentLevelGames || tournamentLevelGames.length === 0) return result;

      tournamentLevelGames = tournamentLevelGames.filter(current => gameIDs.includes(current.id));
      result = Array.from(new Set(result.concat(tournamentLevelGames)));
      return result;
    }, []);
  }

  getGameFromAvailableGames() {
    throw new Error('Method getGameFromAvailableGames not implemented');
  }

  beforeHandler() {}

  beforeGameAddHandler() {}

  nonEmptyGameAddHandler() {}

  sortAvailableGames(availableGames) {
    return availableGames;
  }

  filterAvailableGames(availableGames) {
    return availableGames;
  }

  getAvailableTournaments(tournaments, datetime) {
    return tournaments.filter(tournament =>
      moment(datetime)
        .isBetween(tournament.startDateGames, tournament.endDateGames, null, '[]'),
    );
  }
}
