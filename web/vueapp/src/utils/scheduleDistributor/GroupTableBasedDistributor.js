import Distributor from './Distributor';

function rotateArray(array) {
  const top = [...array[0]];
  const bottom = [...array[1]];
  top.splice(1, 0, bottom[0]);
  bottom.shift();
  bottom.push(top.pop());
  return [top, bottom];
}

function toRotatingArray(array) {
  const top = [];
  const bottom = [];
  for (let i = 0; i < array.length; i += 1) {
    if (i < (array.length / 2)) {
      top.push(array[i]);
    } else {
      bottom.push(array[i]);
    }
  }
  if (bottom.length < top.length) {
    bottom.push(null);
  }
  return [top, bottom];
}

function pairingTable(arr) {
  const result = [];
  const hasByeTeam = arr.length % 2 !== 0;
  const n = hasByeTeam ? arr.length : (arr.length - 1);
  let array = toRotatingArray(arr);

  for (let i = 0; i < n; i += 1) {
    const currentRound = [];

    for (let j = 0; j < array[0].length; j += 1) {
      const left = array[0][j];
      const right = array[1][j];
      if (left && right) {
        currentRound.push(`${left}_${right}`);
      }
    }

    array = rotateArray(array);
    result.push(currentRound);
  }

  return [].concat(...result);
}

function getPairingTableForGroup(group) {
  return pairingTable(group.teams);
}

function getGroups(games, gameByTeamLookup) {
  const groups = games.reduce((obj, game) => {
    if (!obj[game.groupID.toString()]) {
      obj[game.groupID.toString()] = {
        ...game.group,
        teams: [game.teamAHash, game.teamBHash],
      };
    } else {
      const teams = obj[game.groupID.toString()].teams;
      teams.push(game.teamAHash);
      teams.push(game.teamBHash);
      obj[game.groupID.toString()].teams = [...new Set(teams)];
    }
    return obj;
  }, {});
  Object.keys(groups).forEach((key) => {
    const group = groups[key];
    group.pairingTable = getPairingTableForGroup(group);
    group.games = group.pairingTable.map((gameKey, index) => ({
      ...gameByTeamLookup[gameKey],
      indexWithinGroup: index,
    }));
    groups[key] = group;
  });
  return groups;
}

function getGameByTeamIDLookup(games) {
  return games.reduce((obj, game) => {
    obj[[game.teamAHash, game.teamBHash].join('_')] = game;
    obj[[game.teamBHash, game.teamAHash].join('_')] = game;
    return obj;
  }, {});
}

function getCurrentGroups(groups, tournaments, currentLevel, datetime) {
  const availableTournamentIDs = this.getAvailableTournaments(tournaments, datetime)
    .map(tournament => tournament.id.toString());
  return Object.values(groups).filter((group) => {
    const level = currentLevel[group.tournamentID.toString()];
    if (!level) return false;
    return availableTournamentIDs.includes(group.tournamentID.toString()) &&
      group.level.toString() === level.toString();
  });
}

export default class GroupTableBasedDistributor extends Distributor {
  getAvailableGames(games, tournaments, datetime, tournamentLevelLookup, currentLevels) {
    const currentGroups = getCurrentGroups
      .apply(this, [this.groups, tournaments, currentLevels, datetime])
      .map(group => ({ ...group, games: [...group.games] }));
    const availableGames = [];
    const countGames = currentGroups
      .map(group => group.games.length)
      .reduce((acc, val) => acc + val);
    let groupIndex = 0;
    while (availableGames.length < countGames) {
      if (currentGroups[groupIndex].games.length > 0) {
        const game = currentGroups[groupIndex].games.shift();
        availableGames.push(game);
      }
      groupIndex = (groupIndex + 1) % currentGroups.length;
    }

    return availableGames;
  }

  beforeHandler(games) {
    const gameByTeamLookup = getGameByTeamIDLookup(games);
    this.groups = getGroups(games, gameByTeamLookup);
  }

  getGameFromAvailableGames(availableGames) {
    return availableGames[0];
  }

  filterAvailableGames(availableGames, result) {
    const resultIDs = result.map(game => game.id);
    return availableGames.filter(game => !resultIDs.includes(game.id));
  }
}
