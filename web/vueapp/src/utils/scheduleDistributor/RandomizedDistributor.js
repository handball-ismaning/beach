import Distributor from './Distributor';

function popRandomFromArray(arr) {
  return arr.splice(Math.floor(Math.random() * arr.length), 1)[0];
}

export default class RandomizedDistributor extends Distributor {
  getAvailableGames(games, tournaments, datetime, tournamentLevelLookup, currentLevels) {
    return super.getAvailableGames(
      games,
      tournaments,
      datetime,
      tournamentLevelLookup,
      currentLevels,
    );
  }

  beforeGameAddHandler() {
    this.concurrentPlayingTeams = new Set();
  }

  filterAvailableGames(availableGames) {
    return availableGames.filter(current =>
      !this.concurrentPlayingTeams.has(current.teamAHash)
      && !this.concurrentPlayingTeams.has(current.teamBHash));
  }

  getGameFromAvailableGames(availableGames) {
    return popRandomFromArray(availableGames);
  }

  nonEmptyGameAddHandler(game) {
    this.concurrentPlayingTeams.add(game.teamAHash);
    this.concurrentPlayingTeams.add(game.teamBHash);
  }
}
