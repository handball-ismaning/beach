import moment from 'moment';
import Vue from 'vue';

export function parseDate(dateString) {
  return dateString ? moment(dateString, [moment.ISO_8601, 'DD.MM.YYYY']) : undefined;
}

export function parseDateToISOString(date) {
  return date ? date.toISOString() : undefined;
}

export function getDateTimeByKey(obj, key) {
  if (!obj || !key || !obj[key] || !moment(obj[key]).isValid()) {
    return {
      date: undefined,
      time: undefined,
    };
  }
  const datetime = obj[key];
  return {
    date: datetime.format('DD.MM.YYYY'),
    time: datetime.format('HH:mm'),
  };
}

export function checkObjectEmpty(obj) {
  if (!obj) return true;
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}

export function arrayUnion(arr1, arr2, equalityFunc) {
  const union = [].concat(arr1);

  arr2.forEach((obj) => {
    const index = arr1.findIndex(single => equalityFunc(single, obj));
    if (index < 0) {
      union.push(obj);
    } else {
      union[index] = obj;
    }
  });

  return union;
}

export function parseJwt(token) {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
}


export function displayDateRange(startDate, endDate) {
  startDate = parseDate(startDate);
  endDate = parseDate(endDate);
  if (!startDate || !endDate) return undefined;

  if (startDate.isSame(endDate, 'day')) {
    return startDate.format('DD.MM.YYYY');
  }
  return `${startDate.format('DD.MM.YYYY')} - ${endDate.format('DD.MM.YYYY')}`;
}

export function getTournamentCompleteName(tournament) {
  if (tournament.gender === 'mixed') return tournament.name;
  return `${tournament.name} - ${Vue.i18n.t(`general.gender.${tournament.gender}`)}`;
}

export function transformObjectConditionally(obj, transform) {
  if (!obj) return undefined;
  if (typeof obj === 'object') {
    return transform(obj);
  }
  return obj;
}

export function transformArrayConditionally(arr, transform) {
  if (!arr || arr.length === 0) return [];
  return arr.map(single => transformObjectConditionally(single, transform));
}

export function randomNumberFromInterval(min, max) {
  return Math.floor(Math.random() * ((max - min) + 1)) + min;
}

export function uuid() {
  return ((moment().valueOf() * 10000) + randomNumberFromInterval(0, 9999)).toString();
}

export function uuidFromNumber(number) {
  return (
    (moment().valueOf() * 100000)
    + (number * 10000)
    + randomNumberFromInterval(0, 9999)
  ).toString();
}

export function combineDateTime(date, time) {
  const result = moment(date);
  const timeArr = time.split(':');
  result.hours(timeArr[0]);
  result.minutes(timeArr[1]);
  return result.startOf('minute');
}

export function getTeamHash(team) {
  if (team.referenceTeamID) {
    return `${team.tournamentID}-${team.referenceTeamID}`;
  }
  if (team.referenceGroupID && team.place) {
    return `${team.tournamentID}-${team.referenceGroupID}-${team.place}`;
  }
  return undefined;
}

export function generateIDLookup(arr) {
  return arr.reduce((acc, obj) => {
    acc[obj.id] = obj;
    return acc;
  }, {});
}

export function updateOrAddObjToArray(arr, obj) {
  let added = false;
  arr = arr.map((current) => {
    if (current.id !== obj.id) return current;
    added = true;
    return obj;
  });
  if (!added) {
    arr.push(obj);
  }
  return arr;
}

export function downloadObjectAsJson(exportObj, exportName) {
  const dataStr = `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(exportObj))}`;
  const downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute('href', dataStr);
  downloadAnchorNode.setAttribute('download', `${exportName}.json`);
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
}

