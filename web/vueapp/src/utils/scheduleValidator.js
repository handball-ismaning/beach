import { getTeamHash } from '@/utils/helpers';

function mergeObjects(a, b) {
  const result = a;
  Object.keys(b).forEach((key) => {
    if (result[key]) {
      result[key] = Array.from(new Set([...result[key], ...b[key]]));
    } else {
      result[key] = b[key];
    }
  });
  return result;
}

function groupBy(xs, key) {
  return xs.reduce((rv, x) => {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
}

function addErrorForNumber(errors, gameNumber, key) {
  if (errors[gameNumber]) {
    errors[gameNumber].push(key);
  } else {
    errors[gameNumber] = [key];
  }
  errors[gameNumber] = Array.from(new Set(errors[gameNumber]));
  return errors;
}

/**
 * Validate that a team does not play at the same time twice
 * @param   {Array}  games
 * @returns {Object}
 */
function validateGamesDuplicateTeams(games) {
  let errors = {};

  const groupedGames = groupBy(games.map(current => ({
    ...current,
    datetimeString: current.datetime.toISOString(),
  })), 'datetimeString');

  Object.keys(groupedGames).forEach((datetime) => {
    const group = groupedGames[datetime];

    const teams = group.reduce((arr, game) => {
      if (game.teamA) {
        arr.push({
          hash: getTeamHash(game.teamA),
          game: game.number,
        });
      }
      if (game.teamB) {
        arr.push({
          hash: getTeamHash(game.teamB),
          game: game.number,
        });
      }
      return arr;
    }, []);

    const counts = {};
    teams.forEach((team) => {
      const before = counts[team.hash];
      if (before && before.games) {
        counts[team.hash] = {
          count: before.count + 1,
          games: before.games.add(team.game),
        };
      } else {
        counts[team.hash] = {
          count: 1,
          games: new Set([team.game]),
        };
      }
    });

    Object.values(counts).forEach((single) => {
      if (single.count > 1) {
        single.games.forEach((game) => {
          errors = addErrorForNumber(errors, game, 'team_collision');
        });
      }
    });
  });

  return errors;
}

/**
 * Validate that a reference has all games, that are needed, before it is used
 * @param   {Array}  games
 * @returns {Object}
 */
function validateGamesGroupStructure(games) {
  let errors = {};

  const lastGameNumberOfGroup = {};

  games.forEach((game) => {
    if (lastGameNumberOfGroup[game.groupID]) {
      lastGameNumberOfGroup[game.groupID] =
        Math.max(lastGameNumberOfGroup[game.groupID], game.number);
    } else {
      lastGameNumberOfGroup[game.groupID] = game.number;
    }
  });

  games.forEach((game) => {
    [game.teamA, game.teamB].forEach((team) => {
      if (team && team.referenceGroupID &&
        lastGameNumberOfGroup[team.referenceGroupID] >= game.number) {
        errors = addErrorForNumber(errors, game.number, 'group_reference_structure');
      }
    });
  });

  return errors;
}

/**
 * Validate that a game of a tournament does not start outside of tournament dates
 * @param   {Array}  games
 * @returns {Object}
 */
function validateGamesTournamentTime(games) {
  let errors = {};

  games.forEach((game) => {
    const tournament = game.tournament;
    if (!tournament) return;
    if (!game.datetime.isBetween(tournament.startDateGames, tournament.endDateGames, null, '[)')) {
      errors = addErrorForNumber(errors, game.number, 'game_time_tournament');
    }
  });

  return errors;
}

/**
 * Validate game schedule and return an object, that contains errors
 * @param   {Array}  games
 * @returns {Object}
 */
export default function validateSchedule(games) {
  if (!games || games.length === 0) return {};
  let errors = {};

  errors = mergeObjects(errors, validateGamesDuplicateTeams(games));
  errors = mergeObjects(errors, validateGamesGroupStructure(games));
  errors = mergeObjects(errors, validateGamesTournamentTime(games));

  return errors;
}
