import RandomizedDistributor from './scheduleDistributor/RandomizedDistributor';
import GroupTableBasedDistributor from './scheduleDistributor/GroupTableBasedDistributor';

/**
 * Distribute the games randomly across the schedule, while maintaining tournament & group structure
 * @param   {Array}  games
 * @param   {Array}  tournaments
 * @param   {Object} event
 * @param   {String} strategy
 * @returns {Array}
 */
export default function distributeGames(games, tournaments, event, strategy) {
  let distributor;
  if (strategy === 'random') {
    distributor = new RandomizedDistributor({});
  } else if (strategy === 'group-table') {
    distributor = new GroupTableBasedDistributor({});
  }
  if (!distributor) {
    throw new Error(`Distribution-Strategy: ${strategy} is unkown`);
  }
  return distributor.distributeGames(games, tournaments, event);
}
