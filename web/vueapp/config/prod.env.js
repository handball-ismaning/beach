module.exports = {
  NODE_ENV: '"production"',
  API_LOCATION: '"/api/v1"',
  SENTRY_DSN: '"https://23fc94660b684c5696e2c692e61c5844@sentry.io/1251205"',
  GA_CODE: '"UA-98245311-1"',
};
